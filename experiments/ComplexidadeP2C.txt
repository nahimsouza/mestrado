Complexidade P2C

(1) Particionamento por classes

    > separar as amostras em classes 
        -> O(N) onde N é o número total de amostras

    > para cada classe  
      > escolher o melhor k com a tecnica do cotovelo (aplica kmedias 10 vezes para cada k)
        -> sendo Rk = numero de execuções do Kmeans (quantas vezes será repetido)
        -> o valor de K varia de Kmin..Kmax, portanto Rk = Kmax - Kmin + 1
        -> Kmedias sera repetido 10 x Rk para o método do cotovelo -> O(Rk x Kmedias**)
    
      > aplicar k-medias (repete o kmedias 50 vezes com k fixo)
        -> O(Kmedias** x 50)

(2) União de clusters

    > monta um grafo completo (Vertices=clusters, Arestas=fator de união)

        - A complexidade para calcular a sobreposição entre os cluster C1 e C2 é: O(2*NC1 + 2*NC2), onde NCi é o número de amostras no cluster i
        - A complexidade para calcular a proporção uma constante O(1)
        - O fator de uniao será calculado NA vezes, onde NA é o número de arestas
            => NA = ((1 + NV-1)*(NV-1)/2 = ((NV^2) - NV)/2 onde NV é o número de vértices
            => Complexidade para calcular fator de união = O(NA) = O(((NV^2) - NV)/2)

    > encontra componentes conexos (os clusters que serão unidos)
        - Complexidade para encontrar componentes conexos = O(NV + NA) = O(NV + ((NV^2) - NV)/2) = O(2*NV + NV^2 - NV) = O(NV^2 + NV)

(3) Treinamento

    > Para cada grupo, treina classificador local
       - O(NG * TCL) onde NG é o número de grupos e TCL a complexidade do treinamento do classificador

(4) Classificação

    > Selecionar o classificador mais próximo do centroide
       - O(NC) onde NC é o número de centroides

    > Classifica a amostra
       - O(NT * CL) onde CL é a complexidade da classificacao de 1 amostra e NT é o número de amostras de teste

