This folder contains the datasets downloaded from LASID (http://lasid.sor.ufscar.br/clustersEvaluationBenchmark/)

There are artificial and real datasets. The original data is on .txt files and the true partitions are saved in .clu files.