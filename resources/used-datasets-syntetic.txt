DATASETS INFO:

dataset: 2blobs, 
 -samples: 500, 
 -features: 2, 
 -classes: 2, 
 -balanced: True

dataset: 2blobs4classes, 
 -samples: 500, 
 -features: 2, 
 -classes: 4, 
 -balanced: True

dataset: 2globs, 
 -samples: 420, 
 -features: 2, 
 -classes: 2, 
 -balanced: True

dataset: 3blobs, 
 -samples: 500, 
 -features: 2, 
 -classes: 3, 
 -balanced: True

dataset: 4blobs2classes, 
 -samples: 500, 
 -features: 2, 
 -classes: 2, 
 -balanced: True

dataset: 4blobs3classes, 
 -samples: 500, 
 -features: 2, 
 -classes: 3, 
 -balanced: True

dataset: 4clusters, 
 -samples: 500, 
 -features: 2, 
 -classes: 4, 
 -balanced: True

dataset: ds2c2sc13-1, 
 -samples: 588, 
 -features: 2, 
 -classes: 13, 
 -balanced: True

dataset: engyTime, 
 -samples: 4096, 
 -features: 2, 
 -classes: 2, 
 -balanced: True

dataset: gaussian-4classes, 
 -samples: 500, 
 -features: 2, 
 -classes: 4, 
 -balanced: True

dataset: monkey-4, 
 -samples: 4000, 
 -features: 2, 
 -classes: 2, 
 -balanced: True

DATASETS BY CATEGORY:

 balanced: ['2blobs', '2blobs4classes', '2globs', '3blobs', '4blobs2classes', '4blobs3classes', '4clusters', 'ds2c2sc13-1', 'engyTime', 'gaussian-4classes', 'monkey-4']

 unbalanced: []

 binary: ['2blobs', '2globs', '4blobs2classes', 'engyTime', 'monkey-4']

 multiclass: ['2blobs4classes', '3blobs', '4blobs3classes', '4clusters', 'ds2c2sc13-1', 'gaussian-4classes']

 many_features: []

 few_features: ['2blobs', '2blobs4classes', '2globs', '3blobs', '4blobs2classes', '4blobs3classes', '4clusters', 'ds2c2sc13-1', 'engyTime', 'gaussian-4classes', 'monkey-4']

 many_samples: ['engyTime', 'monkey-4']

 few_samples: ['2blobs', '2blobs4classes', '2globs', '3blobs', '4blobs2classes', '4blobs3classes', '4clusters', 'ds2c2sc13-1', 'gaussian-4classes']
