"""
=========================
Machine Learning Wrapper
=========================

It is a wrapper for clustering and classification methods used
implemented by scikit-learning library.

"""

from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.dummy import DummyClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.metrics import silhouette_score
from sklearn.metrics.pairwise import pairwise_distances
from scipy.spatial import distance

import copy
import os
import numpy as np
import pandas as pd

import connected_components as graph
import parameters

from logger import log

if parameters.PLOT_2D_DATA:
    import plot_wrapper as plw
    import matplotlib.pyplot as plt
    from matplotlib.colors import ListedColormap    


def train_classifier(method, features, labels):
    """
    Wrapper to train different classifiers of sklearn
    """

    log("Training classifier (%s)..." % method, parameters.LOG_SOFT_DEBUG)

    params = {}
    number_of_samples = len(labels)

    classifier = None

    if number_of_samples < parameters.MIN_ELEMENTS_PER_CLUSTER:
        log("WARNING: Number of elements is lower than expected!!", parameters.LOG_WARNING)
        log("WARNING: Using Dummy Classifier...", parameters.LOG_WARNING)

        method = parameters.CLASSIFIER_DUMMY
        classifier = DummyClassifier(strategy="most_frequent")
    else:
        if method == parameters.CLASSIFIER_KNN:
            param_grid = [{'n_neighbors': parameters.GRID_KNN_RANGE, 'weights': ['uniform', 'distance']}]
            classifier = GridSearchCV(KNeighborsClassifier(), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      verbose=parameters.GRID_VERBOSE,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_GAUSSIAN_NB:
            classifier = GridSearchCV(GaussianNB(),[{}],
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      verbose=parameters.GRID_VERBOSE,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_RBF_SVM:
            param_grid = [{'kernel': ['rbf'], 'C': parameters.GRID_RANGE3, 'gamma': parameters.GRID_RANGE3}]
            classifier = GridSearchCV(SVC(max_iter=parameters.GRID_SVC_MAX_ITER), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      verbose=parameters.GRID_VERBOSE,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_LINEAR_SVM:
            param_grid = [{'kernel': ['linear'], 'C': parameters.GRID_RANGE3}]
            classifier = GridSearchCV(SVC(max_iter=parameters.GRID_SVC_MAX_ITER), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      verbose=parameters.GRID_VERBOSE,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_DT:
            param_grid = [{'criterion': ['gini','entropy'], 'max_features': ['sqrt','log2']}]
            classifier = GridSearchCV(DecisionTreeClassifier(), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      verbose=parameters.GRID_VERBOSE,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_BERNOULLI_NB:
            param_grid = [{'alpha': parameters.GRID_RANGE7,  'binarize': parameters.GRID_BIN_RANGE}]
            classifier = GridSearchCV(BernoulliNB(), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_MULTINOMIAL_NB:
            param_grid = [{'alpha': parameters.GRID_RANGE7}]
            classifier = GridSearchCV(MultinomialNB(), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_POLYNOMIAL_SVM:
            param_grid = [{'kernel': ['poly'], 'C': parameters.GRID_RANGE3, 'gamma': parameters.GRID_RANGE3,
                             'degree': parameters.GRID_DEGREE_RANGE}]
            classifier = GridSearchCV(SVC(max_iter=parameters.GRID_SVC_MAX_ITER), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      verbose=parameters.GRID_VERBOSE,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_LINEAR_REGRESSION:
            param_grid = [{}]
            classifier = GridSearchCV(LinearRegression(), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_LOGISTIC_REGRESSION:
            param_grid = [{'C': parameters.GRID_RANGE3}]
            classifier = GridSearchCV(LogisticRegression(), param_grid,
                                      cv=parameters.GRID_KFOLD,
                                      scoring=parameters.GRID_SCORING,
                                      n_jobs=-1)
        elif method == parameters.CLASSIFIER_DUMMY:
            log("NOTE: Using DummyClassifier", parameters.LOG_SOFT_DEBUG)
            classifier = DummyClassifier(strategy="most_frequent")
        else:
            log("ERROR: Method not implemented: " + method, parameters.LOG_ERROR)
            return

    score = 0.0
    best_classifier = None

    if classifier != None:
        classifier.fit(features, labels)

        if method == parameters.CLASSIFIER_DUMMY:
            best_classifier = classifier
            score = best_classifier.score(features, labels)
        else:
            best_classifier = classifier.best_estimator_
            score = classifier.best_score_  # Score of best_estimator on the left out data.

    log("CLASSIFIER(%s) SCORE: %.3f" % (method, score), parameters.LOG_SOFT_DEBUG)

    result = {
        "method": method,
        "clf": best_classifier,
        "score": score,
        "number_of_samples": number_of_samples,
        "performance_on_selection": {}
    }

    return result


def classify(classifier, features):
    return classifier.predict(features)

def classify_probability(classifier, features):
    return classifier.predict_proba(features)

def cluster(number_of_clusters, features):
    log("Clustering...", parameters.LOG_SOFT_DEBUG)
    features_scaled = MinMaxScaler().fit_transform(features)
    kmeans = KMeans(init='k-means++',
                    n_clusters=number_of_clusters,
                    n_init=parameters.KMEANS_MAX_ITERATIONS,
                    n_jobs=-1)
    kmeans.fit(features_scaled)

    # Check the min number of elements per cluster
    for label in set(kmeans.labels_):
        number_of_elements = list(kmeans.labels_).count(label)

        if number_of_elements < parameters.MIN_ELEMENTS_PER_CLUSTER and number_of_clusters > 1:
            log("WARNING: One or more clusters has less than "
                + str(parameters.MIN_ELEMENTS_PER_CLUSTER) + " elements", parameters.LOG_WARNING)
            log("WARNING: Clustering again with " + str(number_of_clusters - 1) + " clusters", parameters.LOG_WARNING)
            return cluster(number_of_clusters - 1, features_scaled)


    # Print the elements per cluster
    for label in set(kmeans.labels_):
        number_of_elements = list(kmeans.labels_).count(label)
        log("Num samples in cluster " + str(label) + ": " + str(number_of_elements), parameters.LOG_SOFT_DEBUG)

    # TODO: Calcular metricas de dispersao: media, desvio e variancia

    silhouette = None
    if number_of_clusters > 1:
        silhouette = -99 #silhouette_score(features_scaled, kmeans.labels_)

    # get centroid for k=1
    centroids = [np.mean(features_scaled, axis=0)]
    if number_of_clusters > 1:
        centroids = kmeans.cluster_centers_


    pairwise_intracluster = pairwise_distance_intra_cluster(features, kmeans.labels_)

    result = {
        "number_of_clusters": number_of_clusters,
        "labels": kmeans.labels_,
        "centroid": centroids,
        "silhouette": silhouette,
        "average_centroid_distance": average_centroid_distance(features, kmeans.labels_, centroids),
        "pairwise_distance_between_centroids": pairwise_distance_between_centroids(centroids),
        "pairwise_distance_intra_cluster": pairwise_intracluster["distance"],
        "pairwise_distance_intra_cluster_stdev": pairwise_intracluster["stdev"],
        "pairwise_distance_intra_cluster_variance": pairwise_intracluster["variance"],
    }

    return result

def get_best_k_using_gap(data, nrefs=3, max_clusters=15):
    """
    Calculates KMeans optimal K using Gap Statistic from Tibshirani, Walther, Hastie
    Params:
        data: ndarry of shape (n_samples, n_features)
        nrefs: number of sample reference datasets to create
        max_clusters: Maximum number of clusters to test for
    Returns: best_k

    Adapted from: https://anaconda.org/milesgranger/gap-statistic/notebook
    """

    log("Finding optimal K: nsamples=%d, nrefs=%d, max_clusters=%d" % (len(data), nrefs, max_clusters), parameters.LOG_SOFT_DEBUG)

    gaps = np.zeros((len(range(1, max_clusters)),))
    resultsdf = pd.DataFrame({'clusterCount':[], 'gap':[]})
    for gap_index, k in enumerate(range(1, max_clusters)):

        # Holder for reference dispersion results
        refDisps = np.zeros(nrefs)

        # For n references, generate random sample and perform kmeans getting resulting dispersion of each loop
        for i in range(nrefs):

            # Create new random reference set
            randomReference = np.random.random_sample(size=data.shape)

            # Fit to it
            km = KMeans(n_clusters=k, init='k-means++', n_init=parameters.KMEANS_MAX_ITERATIONS, n_jobs=-1)
            km.fit(randomReference)

            refDisp = km.inertia_
            refDisps[i] = refDisp

        # Fit cluster to original data and create dispersion
        km = KMeans(n_clusters=k, init='k-means++', n_init=parameters.KMEANS_MAX_ITERATIONS, n_jobs=-1)
        km.fit(data)

        origDisp = km.inertia_

        # Calculate gap statistic
        gap = np.log(np.mean(refDisps)) - np.log(origDisp)

        # Assign this loop's gap statistic to gaps
        gaps[gap_index] = gap

        resultsdf = resultsdf.append({'clusterCount':k, 'gap':gap}, ignore_index=True)

    best_k = gaps.argmax() + 1 # Plus 1 because index of 0 means 1 cluster is optimal, index 2 = 3 clusters are optimal
    log("Best K = " + str(best_k), parameters.LOG_SOFT_DEBUG)
    return best_k

def get_best_k_using_elbow(data, max_clusters):
    """
    Gets the best K using elbow method
    """

    inertias = np.zeros((len(range(1, max_clusters)),))
    diffs = np.zeros((len(range(1, max_clusters)),))
    perc_diffs = np.zeros((len(range(1, max_clusters)),))
    
    best_k = parameters.MIN_NUMBER_OF_CLUSTERS
    resultsdf = pd.DataFrame({'clusterCount':[], 'inertia':[], 'diffs':[], 'perc_diffs':[]})

    for index, k in enumerate(range(parameters.MIN_NUMBER_OF_CLUSTERS, max_clusters)):

        # Fit cluster to original data and create dispersion
        km = KMeans(k)
        km.fit(data)

        inertias[index] = km.inertia_

        if index > 0:
            diffs[index] = abs(km.inertia_ - inertias[index-1])
            perc_diffs[index] = (diffs[index]/inertias[index-1])*100
        else:
            diffs[index] = km.inertia_
            perc_diffs[index] = 100

        # just for debbugging
        resultsdf = resultsdf.append({'clusterCount':k, 'inertia': km.inertia_,
                    'diffs':diffs[index], 'perc_diffs':perc_diffs[index]}, ignore_index=True)

        best_k = k
        if perc_diffs[index] < parameters.ELBOW_PERCENTAGE:
            break

    log("Best K = " + str(best_k), parameters.LOG_SOFT_DEBUG)
    return best_k


def get_optimal_k(data, max_clusters=parameters.MAX_NUMBER_OF_CLUSTERS):

    result = None

    if parameters.USE_GAP_STATISTICS:
        result = get_best_k_using_gap(data, 3, max_clusters)
    else:
        result = get_best_k_using_elbow(data, max_clusters)

    return result

def separate_samples_by_label(samples, labels, centroids=[]):
    """
    Separates samples according to labels. If 'centroids' is not empty,
    it means that centroids must be returned for each set of samples.

    Returns:
        A structure like this:
        partition = {
            "label": {
                "samples":[[]]
                "centroid":[]
            },
        }
    """

    partition = {}
    for i in range(0, len(labels)): 
        label = str(labels[i])
        if not label in partition:
            partition[label] = {"samples":[], "centroid":[]}

        partition[label]["samples"].append(samples[i])
        if len(centroids) > 0:
            partition[label]["centroid"] = centroids[int(label)]
        else:
            partition[label]["centroid"] = get_centroid(partition[label]["samples"])

    return partition

def get_weight(small_cluster, big_cluster):
    """
    The weight between clusters is calculated using the theory of
    gravitational forces i.e. F = G * (m1*m2) / d**2

    We will consider G = 1 for our scenario.
    """

    # The mass is the size of each cluster
    m1 = len(small_cluster['samples'])
    m2 = len(big_cluster['samples'])
    d = distance.euclidean(small_cluster['centroid'], big_cluster['centroid'])
    G = 1

    weight = G * (m1*m2) / d**2

    log("m1: %.2f, m2: %.2f, d**2: %.2f" % (m1, m2, d**2), parameters.LOG_SOFT_DEBUG)
    log("weight: " + str(weight), parameters.LOG_SOFT_DEBUG)

    return weight

def get_centroid_distance(small_cluster, big_cluster):
    """
    Returns the euclidean distance between centoids
    """
    return distance.euclidean(small_cluster['centroid'], big_cluster['centroid'])

def get_centroid(samples):
    centroid = np.mean(samples, axis=0)
    return centroid

def merge_clusters(small_cluster, big_cluster):

    samples = []
    samples.extend(small_cluster["samples"])
    samples.extend(big_cluster["samples"])

    centroid = get_centroid(samples)

    cluster = {
        "samples": samples,
        "centroid": centroid
    }

    return cluster

def get_small_clusters(partition):
    small_clusters = {}
    for label in partition:
        if len(partition[label]['samples']) < parameters.MIN_ELEMENTS_PER_CLUSTER:
            # log("partition labeled: %s, len: %d" % (label, len(partition[label]['samples'])))
            small_clusters[label] = copy.deepcopy(partition[label])

    log("There are %d small clusters" % (len(small_clusters)), parameters.LOG_SOFT_DEBUG)
    log("SMALL CLUSTERS: " + str(small_clusters), parameters.LOG_FULL_DEBUG)

    return small_clusters

def remove_small_clusters(samples, kmeans):
    """
    Remove small clusters merging them with bigger ones and returns a new partition
    """

    partition = separate_samples_by_label(samples, kmeans.labels_, kmeans.cluster_centers_)
    small_clusters = get_small_clusters(partition)

    while len(small_clusters) > 0:

        log("REMOVING SMALL CLUSTERS...", parameters.LOG_SOFT_DEBUG)

        # the weights are the intensity of atraction between clusters
        # they are calculated only for small clusters, because they can be merged on bigger clusters
        new_partition = {}
        for label in small_clusters:
            max_weight = 0  # in case of using minimum weight to remove small clusters
            min_distance = float("inf") # represents infinite, in case of using distance criteria to merge
            cluster_label = None
            # log("Label: " + str(label))
            for lbl in partition:
                if label != lbl:
                    if parameters.USE_WEIGHT == True:
                        weight = get_weight(small_clusters[label], partition[lbl])
                        if weight > parameters.WEIGHT and weight > max_weight:
                            cluster_label = lbl
                            max_weight = weight
                    else:
                        # uses minimum distance
                        # log("Using centroid distance", parameters.LOG_FULL_DEBUG)
                        dist = get_centroid_distance(small_clusters[label], partition[lbl])
                        if dist < min_distance:
                            cluster_label = lbl
                            min_distance = dist

            if cluster_label != None:
                # adds the merged cluster to the new partition
                new_partition[cluster_label] = merge_clusters(small_clusters[label], partition[cluster_label])
            else:
                log("Warning: one small cluster was not merged!", parameters.LOG_WARNING)
                new_partition[label] = small_clusters[label]

        # adds the remaining clusters:
        for label in partition:
            if (label not in small_clusters) and (label not in new_partition):
                new_partition[label] = partition[label]

        # update values to new iteration
        partition = copy.deepcopy(new_partition)
        small_clusters = get_small_clusters(partition)

        log("> Total small clusters: %d" % (len(small_clusters)), parameters.LOG_SOFT_DEBUG)

    # adds max_distance_from_centroid on result
    for label in partition:
        partition[label]["max_distance_from_centroid"] = get_max_distance_from_centroid(partition[label]["samples"])

    log("NEW PARTITION", parameters.LOG_FULL_DEBUG)
    log(partition, parameters.LOG_FULL_DEBUG)

    return partition

def make_intersections(clusters_by_class):
    """
    This function must receive the clusters formed insisde each
    class. And after that make the merge between these partitions.

    Expected input for binary class:
    clusters_by_class =
    {
        "1" : {
            "0":{
                "labels": ["1",...]
                "samples":[],
                "centroid":[]
            },
             "1": {
                 "labels": ["1",...]
                 "samples":[],
                 "centroid":[]
             },
             "4": {
                 "labels": ["1",...]
                 "samples":[],
                 "centroid":[]
             },
             "3": {
                 "labels": ["1",...]
                 "samples":[],
                 "centroid":[]
             }
         },
         "2" : {
             "0":{
                 "labels": ["2",...]
                 "samples":[],
                 "centroid":[]
             },
             "1": {
                 "labels": ["2",...]
                 "samples":[],
                 "centroid":[]
             }
         },
    }

    Expected output:
    intersection = {
        "0":{
            "labels": ["1",...]
            "samples":[],
            "centroid":[]
        },
        "1": {
            "labels": ["1",...]
            "samples":[],
            "centroid":[]
        },
        "2": { # merge among clusters 2 and 3 (from class 1), and cluster 0 (from class 2)
            "labels":["1",...,"2",...], # merge between classes 1 and 2
            "samples":[],
            "centroid":[]
        },
        "3": {
            "labels": ["1",...]
            "samples":[],
            "centroid":[]
        },
    }
    """

    start_time = os.times()

    log("Make Intersections...", parameters.LOG_SOFT_DEBUG)

    intersection = {}

    # Get the maximum number of samples in one cluster
    max_number_of_samples = 0
    for label in clusters_by_class:
        partition = clusters_by_class[label]
        
        for cluster_id in partition:
            if len(partition[cluster_id]["samples"]) > max_number_of_samples:
                max_number_of_samples = len(partition[cluster_id]["samples"])

        # Calculate the cluster density => normalized number of samples
        # Density = [0,1]
        for cluster_id in partition:
            partition[cluster_id]["density"] = float(len(partition[cluster_id]["samples"])/max_number_of_samples)
            log("> density: %.2f" % partition[cluster_id]["density"], parameters.LOG_FULL_DEBUG)
    
    # Needs to copy because dictionary can't change during its handling
    aux_clusters_by_class = copy.deepcopy(clusters_by_class)

    # compare 2-by-2 intersection
    for label in clusters_by_class:
        nth_cluster = aux_clusters_by_class.pop(label)
        intersection = pair_intersection(intersection, nth_cluster)

    end_time = os.times()
    
    # cpu_time = user + sys time
    log("CPU TIME - make_intersection: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_ALWAYS)

    return intersection

def make_intersections_using_graph(clusters_by_class):
    """
    This function must receive the clusters formed insisde each
    class. And after that make the merge between these partitions.

    Expected input for binary class:
    clusters_by_class =
    {
        "1" : {
            "0":{
                "labels": ["1",...]
                "samples":[],
                "centroid":[]
            },
             "1": {
                 "labels": ["1",...]
                 "samples":[],
                 "centroid":[]
             },
             "4": {
                 "labels": ["1",...]
                 "samples":[],
                 "centroid":[]
             },
             "3": {
                 "labels": ["1",...]
                 "samples":[],
                 "centroid":[]
             }
         },
         "2" : {
             "0":{
                 "labels": ["2",...]
                 "samples":[],
                 "centroid":[]
             },
             "1": {
                 "labels": ["2",...]
                 "samples":[],
                 "centroid":[]
             }
         },
    }

    Expected output:
    intersection = {
        "0":{
            "labels": ["1",...]
            "samples":[],
            "centroid":[]
        },
        "1": {
            "labels": ["1",...]
            "samples":[],
            "centroid":[]
        },
        "2": { # merge among clusters 2 and 3 (from class 1), and cluster 0 (from class 2)
            "labels":["1",...,"2",...], # merge between classes 1 and 2
            "samples":[],
            "centroid":[]
        },
        "3": {
            "labels": ["1",...]
            "samples":[],
            "centroid":[]
        },
    }
    """

    start_time = os.times()

    # Get the maximum number of samples in one cluster
    max_number_of_samples = 0
    for label in clusters_by_class:
        partition = clusters_by_class[label]
        
        for cluster_id in partition:
            if len(partition[cluster_id]["samples"]) > max_number_of_samples:
                max_number_of_samples = len(partition[cluster_id]["samples"])

        # Calculate the cluster density => normalized number of samples
        # Density = [0,1]
        for cluster_id in partition:
            partition[cluster_id]["density"] = float(len(partition[cluster_id]["samples"])/max_number_of_samples)
            log("> density: %.2f" % partition[cluster_id]["density"], parameters.LOG_FULL_DEBUG)

    clusters = {}
    
    # create a unique dict with all groups from all partitions
    for partition in clusters_by_class:
        for cluster in clusters_by_class[partition]:
            clusters[str(partition + "." + cluster)] = clusters_by_class[partition][cluster]

    # add the vertices on the graph
    cluster_list  = []
    for cluster_id in clusters:
        cluster_list.append(cluster_id)
        clusters[cluster_id]["node"] = graph.Data(cluster_id)

    # add the edges of the graph
    for i in range(0, len(cluster_list)):
        for j in range(i, len(cluster_list)):
            cluster_one = clusters[cluster_list[i]]
            cluster_two = clusters[cluster_list[j]]

            if has_intersection_between_clusters(cluster_one, cluster_two):
                cluster_one["node"].add_link(cluster_two["node"])
                
    # Put all the nodes together in one big set
    intersection_set = set()
    for key in clusters:
        intersection_set.add(clusters[key]["node"])

    # find the connected components
    intersection_result = graph.connected_components(intersection_set)

    # Just to print log
    if (parameters.LOG_LEVEL >= parameters.LOG_SOFT_DEBUG):
        number = 1
        for components in intersection_result:
            names = sorted(node.name for node in components)
            names = ", ".join(names)
            log("Cluster #%i: %s" % (number, names), parameters.LOG_SOFT_DEBUG)
            number += 1

    # final intersection
    intersection = {}

    # make the union between them
    for components in intersection_result:
        
        # result of merging clusters
        merge = {}
        
        for node in components:
            cluster = clusters[node.name]

            if cluster != None:
                if len(merge) == 0:
                    merge = cluster
                    # Important: do not remove, some datasets need this conversion
                    if isinstance(merge["samples"], np.ndarray):
                        merge["samples"] = merge["samples"].tolist()
                else:
                    merge["labels"].extend(cluster["labels"])
                    merge["samples"].extend(cluster["samples"])

        # recalculate the centroid after merge
        merge["centroid"] = get_centroid(merge["samples"])
        merge["max_distance_from_centroid"] = get_max_distance_from_centroid(merge["samples"])

        intersection[str(len(intersection))] = merge
    
    log("> Final Intersection Result: " + str(intersection), parameters.LOG_FULL_DEBUG)

    end_time = os.times()
    
    # cpu_time = user + sys time
    log("CPU TIME - make_intersection_using_graph: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_ALWAYS)

    return intersection

def get_max_distance_from_centroid(samples):
    centroid = get_centroid(samples)
    max_distance = np.max(pairwise_distances(samples, centroid, metric="euclidean"))
    return max_distance

def pair_intersection(partition1, partition2):
    """
    Makes the intersection between the partitions of two classes

    OBS: Remember that the intersections (merge) are made between CLUSTERS,
    not entire partitions!

    Output: This output must be in the same format than make_intersections()
            for two partitions. Example:

            intersection =
            {
                "0":{
                    "labels": ["1",...]
                    "samples":[],
                    "centroid":[]
                },
                "1": {
                    "labels": ["1",...]
                    "samples":[],
                    "centroid":[]
                },
                "2": {
                    "labels":["1",...,"2",...], # merge between classes 1 and 2
                    "samples":[],
                    "centroid":[]
                },
                "3": {
                    "labels": ["1",...]
                    "samples":[],
                    "centroid":[]
                },
            }

    """

    start_time = os.times()

    intersection = {}

    log("PARTITION ONE", parameters.LOG_FULL_DEBUG)
    log(partition1, parameters.LOG_FULL_DEBUG)

    log("PARTITION TWO", parameters.LOG_FULL_DEBUG)
    log(partition2, parameters.LOG_FULL_DEBUG)


    if partition1 == {}:
        intersection = partition2
    elif partition2 == {}:
        intersection = partition1
    else:   
        # a map to keep the graph nodes    
        intersection_nodes = {}

        for cluster_id1 in partition1:
            index1 = "P1_" + str(cluster_id1)
            if not index1 in intersection_nodes:
                intersection_nodes[index1] = graph.Data(index1)

            for cluster_id2 in partition2:
                index2 = "P2_" + str(cluster_id2)
                if not index2 in intersection_nodes:
                    intersection_nodes[index2] = graph.Data(index2)

                if has_intersection_between_clusters(partition1[cluster_id1], partition2[cluster_id2]):
                    intersection_nodes[index1].add_link(intersection_nodes[index2])

        intersection_set = set()
        for key in intersection_nodes:
            intersection_set.add(intersection_nodes[key])

        intersection_result = graph.connected_components(intersection_set)

        for components in intersection_result:
            # result of merging clusters
            merge = {}
            
            for node in components:
                cluster = None

                # get the name after 'Px_'
                if "P1_" in node.name:
                    cluster = partition1.pop(node.name[3:])
                elif "P2_" in node.name:
                    cluster = partition2.pop(node.name[3:])

                if cluster != None:
                    if len(merge) == 0:
                        merge = cluster
                        # Important: do not remove, some datasets need this conversion
                        if isinstance(merge["samples"], np.ndarray):
                            merge["samples"] = merge["samples"].tolist()
                    else:
                        merge["labels"].extend(cluster["labels"])
                        merge["samples"].extend(cluster["samples"])

            # recalculate the centroid after merge
            merge["centroid"] = get_centroid(merge["samples"])
            merge["max_distance_from_centroid"] = get_max_distance_from_centroid(merge["samples"])

            intersection[str(len(intersection))] = merge
    
    log("Pair Intersection Result: " + str(intersection), parameters.LOG_FULL_DEBUG)

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - pair_intersection: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)

    return intersection

def has_intersection_between_clusters(c1, c2):

    start_time = os.times()

    intersection_counter =  0

    cluster1 = c1["samples"]
    cluster2 = c2["samples"]

    max_distance_c1 = c1["max_distance_from_centroid"]
    max_distance_c2 = c2["max_distance_from_centroid"]

    density_c1 = c1["density"]
    density_c2 = c2["density"]

    log("\nmax_distance_c1: %.3f, max_distance_c2: %.3f" % (max_distance_c1, max_distance_c2), parameters.LOG_SOFT_DEBUG)

    centroid_c1 = get_centroid(cluster1)
    centroid_c2 = get_centroid(cluster2)

    has_intersection = False

    if parameters.USE_INTERSECTION_CRITERIA:
        for sample1 in cluster1:
            dist = distance.euclidean(sample1, centroid_c2)
            if dist <= max_distance_c2:
                intersection_counter += 1

        for sample2 in cluster2:
            dist = distance.euclidean(sample2, centroid_c1)
            if dist <= max_distance_c1:
                intersection_counter += 1

        total_samples = len(cluster1) + len(cluster2)
        intersection_percentage = intersection_counter/total_samples
        
        proportion = float(min(density_c1, density_c2)/max(density_c1, density_c2))
        
        intersection_criteria = intersection_percentage / proportion

        log("total_samples (c1 + c2): " + str(total_samples), parameters.LOG_SOFT_DEBUG)
        log("intersection_counter: " + str(intersection_counter), parameters.LOG_SOFT_DEBUG)
        log("density proportion: %.2f" % proportion, parameters.LOG_SOFT_DEBUG)
        log("intersection criteria: %.2f" % proportion, parameters.LOG_SOFT_DEBUG)
        log("intersection_percentage: %.3f\n" % (intersection_percentage), parameters.LOG_SOFT_DEBUG)

        if parameters.INTERSECTION_BY_PROPORTION:
            has_intersection = ((intersection_criteria > parameters.INTERSECTION_CRITERIA_PROPORTION) and (intersection_counter > 0))
        else:
            # Intersection criteria using only percentage
            has_intersection = (intersection_percentage > parameters.INTERSECTION_CRITERIA_PERCENTAGE) 
    else:
        if (distance.euclidean(centroid_c1, centroid_c2) <= max_distance_c1
            or distance.euclidean(centroid_c1, centroid_c2) <= max_distance_c2):
            has_intersection = True
        else:
            has_intersection = False

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - has_intersection_between_clusters: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)

    return has_intersection

def get_centroid(samples):
    return np.mean(samples, axis=0)

def dynamic_cluster(samples):
    """
    This function makes clustering using GAP statistics to find the best K,
    and after that remove or merge clusters that are too small.

    Returns: A dictionary containing the samples and centroid for
             each cluster. Example:

             result = {
                '0': {
                    'samples':[...],
                    'centroid':[...]
                },
                '1': {
                    'samples':[...],
                    'centroid:[...]
                }
            }

    """

    start_time = os.times()

    # cluster and find the best K
    number_of_samples = len(samples)
    max_num_clusters = int(number_of_samples / parameters.MIN_ELEMENTS_PER_CLUSTER)
    max_num_clusters_by_percent = int(number_of_samples * parameters.MIN_ELEMENTS_PER_CLUSTER_PERCENT)

    # define the maximum number of clusters
    max_clusters = parameters.MAX_NUMBER_OF_CLUSTERS

    if parameters.USE_MAX_CLUSTER_DEFAULT == False:
        max_clusters = max_num_clusters_by_percent if max_num_clusters_by_percent > parameters.MIN_ELEMENTS_PER_CLUSTER else max_num_clusters

    best_k = get_optimal_k(samples, max_clusters)
    kmeans = KMeans(init='k-means++', n_clusters=best_k, n_init=parameters.KMEANS_MAX_ITERATIONS, n_jobs=-1)
    kmeans.fit(samples)

    # remove small clusters
    partition = remove_small_clusters(samples, kmeans)

    # workaround to fix cluster labels:
    result = {}
    count = 0
    for key in partition:
        result[str(count)] = partition[key]
        count += 1

    end_time = os.times()
    
    # cpu_time = user + sys time
    log("CPU TIME - dynamic_clustering: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)

    return (result, kmeans)

def cluster_by_class(samples, labels):
    """
    Cluster the samples inside each class and merge them according to
    a criteria of intersection.
    """

    start_time = os.times()

    # Separate samples by class
    samples_by_label = separate_samples_by_label(samples, labels)
    clusters_by_class = {}

    if parameters.PLOT_2D_DATA:
        # Just Ploting 2 GLOBS DATASET
        log("Plotting full dataset")
        for i in range(0,len(samples_by_label)):
            plt.plot(*zip(*samples_by_label[str(i)]["samples"]), 
                        marker=parameters.MARKER_LIST[i % len(parameters.MARKER_LIST)], markeredgecolor="#000000", markeredgewidth="0.7",
                        color=parameters.COLOR_PALETTE[i], linestyle='')

        plt.xlim([0,1])
        plt.ylim([0,1])

        plt.title("Full dataset")
        if parameters.PLOT_SAVE_FIG == True:
            plt.savefig(parameters.PLOT_CURRENT_DIR + "full_dataset.png")
            plt.clf()
        else:
            plt.show()

    # For each class, makes clustering and eliminate small clusters
    for label in samples_by_label:
        samples_list = np.array(samples_by_label[label]['samples'])

        if parameters.PLOT_2D_DATA:
            log("Ploting separated classes...")
            plw.plot_2d_points(samples_list, color=parameters.COLOR_PALETTE[int(label)],
                                marker=parameters.MARKER_LIST[int(label) % len(parameters.MARKER_LIST)],
                                title="Samples from class: " + str(label),
                                save_name="samples_from_class_" + str(label) + ".png")

        # Check if there is a minimum of elements by class
        if len(samples_list) > parameters.MIN_ELEMENTS_PER_CLUSTER*2:
            if not label in clusters_by_class:
                clusters_by_class[label] = {}

            # The clusters generated for class label and the kmeans object (to plot)
            clusters_by_class[label], kmeans = dynamic_cluster(samples_list)

            # print("Clusters for Label " + str(label) + " :: " + str(clusters_by_class[label]))

            centroids = []
            for cluster in clusters_by_class[label]:
                centroids.append(clusters_by_class[label][cluster]["centroid"])
            
            centroids = np.array(centroids)


            # to plot the correct number of clusters
            km = KMeans(n_clusters=len(clusters_by_class[label]),
                        init=centroids, n_init=1)
            km.fit(samples_list)

            for cluster in clusters_by_class[label]:
                number_of_samples = len(clusters_by_class[label][cluster]["samples"])
                clusters_by_class[label][cluster]["labels"] = [str(label)] * number_of_samples

                if parameters.PLOT_2D_DATA:
                    log("Ploting cluster by classes...", parameters.LOG_SOFT_DEBUG)
                    log("Class: " + str(label) + " Cluster: " + str(cluster) + " Size: " + str(number_of_samples), parameters.LOG_SOFT_DEBUG)
                    
                    # To plot the classifier
                    h = 0.002 # step size in the mesh
                    x_min, x_max = (0,1)
                    y_min, y_max = (0,1)
                    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
                       
                    Z = km.predict(np.c_[xx.ravel(), yy.ravel()])
                    
                    # print ("Z" + str(Z))

                    # Put the result into a color plot
                    Z = Z.reshape(xx.shape)
                    plt.contourf(xx, yy, Z, cmap=plt.cm.YlGnBu, alpha=0.4)
                    plt.contour(xx, yy, Z, cmap=ListedColormap(["#090909"]), linewidths=0.7, zorder=10)

                    # Plot centroids
                    # centroids = kmeans.cluster_centers_
                    plt.scatter(centroids[:, 0], centroids[:, 1],
                                marker='x', s=80, linewidths=10,
                                color='w', zorder=10)

                    plt.plot(*zip(*clusters_by_class[label][cluster]["samples"]), 
                                    marker=parameters.MARKER_LIST[int(label) % len(parameters.MARKER_LIST)], #markersize=6, 
                                    markeredgecolor="#000000", markeredgewidth="0.6",
                                    color=parameters.COLOR_PALETTE[int(label)], 
                                    linestyle='')

            log("Clusters for Class: " + str(label) + ", Best K = " + str(len(clusters_by_class[label])), parameters.LOG_SOFT_DEBUG)
            if parameters.PLOT_2D_DATA:
                plt.title("Clusters for Class: " + str(label) + ", Best K = " + str(len(clusters_by_class[label])))
                plt.xlim([0,1])
                plt.ylim([0,1])
                
                if parameters.PLOT_SAVE_FIG == True:
                    plt.savefig(parameters.PLOT_CURRENT_DIR + "cluster_for_class_" + str(label) 
                                + "-best_k_" + str(len(clusters_by_class[label]))  + ".png")
                    plt.clf()
                else:
                    plt.show()

        else:
            log("Warning: Class has less elements than the minumum expected for clustering", parameters.LOG_WARNING)
            clusters_by_class[label] = {
                "0": {
                    "labels": [str(label)] * len(samples_list), # to identify classes during intersections
                    "samples": samples_list,
                    "centroid": get_centroid(samples_list),
                    "max_distance_from_centroid": get_max_distance_from_centroid(samples_list)
                }
            }

    log("Dynamic cluster result", parameters.LOG_FULL_DEBUG)
    log(clusters_by_class, parameters.LOG_FULL_DEBUG)

    clusters_intersection = None

    # Makes new clusters through intersections
    if (parameters.USE_CENTROID_RECALCULATION):
        clusters_intersection = make_intersections(clusters_by_class)
    else:
        clusters_intersection = make_intersections_using_graph(clusters_by_class)

    log("\nRESULT OF CLUSTER INTERSECTION", parameters.LOG_FULL_DEBUG)
    log(clusters_intersection, parameters.LOG_FULL_DEBUG)

    # Organize clusters, calculate metrics and return the clusters
    result = {
        "partition": clusters_intersection,
        "number_of_clusters": len(clusters_intersection),
        "labels": [], # makes no sense for this method, just for keep the pattern
        "centroid": [],
        "silhouette": 0,
        "average_centroid_distance": 0, # average_centroid_distance(features, kmeans.labels_, centroids),
        "pairwise_distance_between_centroids": 0, #pairwise_distance_between_centroids(centroids),
        "pairwise_distance_intra_cluster": 0, #pairwise_intracluster["distance"],
        "pairwise_distance_intra_cluster_stdev": 0, #pairwise_intracluster["stdev"],
        "pairwise_distance_intra_cluster_variance": 0#pairwise_intracluster["variance"],
    }

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - cluster_by_class: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_ALWAYS)


    return result

def cluster_by_centroid(samples, labels, centroids):

    start_time = os.times()

    partition = {}

    # create empty partition
    for centroid in centroids:
        index = str(len(partition))
        partition[index] = {
            "samples":[],
            "centroid": centroid,
            "labels":[],
            "max_distance_from_centroid":0.0
        }


    for index in range(0, len(samples)):
        sample = np.array(samples[index]).astype(np.float)
        nearest = 0 # index of nearest centroid

        for i in range(1, len(partition)):
            d_nearest = distance.euclidean(sample, partition[str(nearest)]["centroid"])
            d_current = distance.euclidean(sample, partition[str(i)]["centroid"])
            if (d_current < d_nearest):
                nearest = i

        partition[str(nearest)]["samples"].append(sample)
        partition[str(nearest)]["labels"].append(labels[index])


    for cluster in partition:
        partition[cluster]["max_distance_from_centroid"] = get_max_distance_from_centroid(partition[cluster]["samples"])

    # FOR COMPATIBILITY
    # Organize clusters, calculate metrics and return the clusters
    result = {
        "partition": partition,
        "number_of_clusters": len(partition),
        "labels": [], # makes no sense for this method, just for keep the pattern
        "centroid": [],
        "silhouette": 0,
        "average_centroid_distance": 0, # average_centroid_distance(features, kmeans.labels_, centroids),
        "pairwise_distance_between_centroids": 0, #pairwise_distance_between_centroids(centroids),
        "pairwise_distance_intra_cluster": 0, #pairwise_intracluster["distance"],
        "pairwise_distance_intra_cluster_stdev": 0, #pairwise_intracluster["stdev"],
        "pairwise_distance_intra_cluster_variance": 0#pairwise_intracluster["variance"],
    }

    end_time = os.times()
    
    # cpu_time = user + sys time
    log("CPU TIME - cluster_by_centroid: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_ALWAYS)


    return result

def dbscan_cluster(features):
    """
    Make clusters using dbscan method
    """

    # TODO: Fazer um 'grid-search' ou algo parecido para encontrar os melhores parametros
    # para eps e mis_samples

    dbscan = DBSCAN(eps=0.3, min_samples=10).fit(features)

    # Number of clusters in labels, ignoring noise if present.
    number_of_clusters = len(set(labels)) - (1 if -1 in labels else 0)

    # Calculate Silhouette
    silhouette = None
    if number_of_clusters > 1:
        silhouette = silhouette_score(features, kmeans.labels_)

    # Print number of samples per cluster
    for l in set(kmeans.labels_):
        log("Num samples in cluster " + str(l) + ": " + str(list(kmeans.labels_).count(l)), parameters.LOG_SOFT_DEBUG)

    result = {
        "number_of_clusters": number_of_clusters,
        "labels": dbscan.labels_,
        "centroid": dbscan.components_, # TODO: Conferir se isso corresponde aos centroides
        "silhouette": silhouette
    }

    return result

def pairwise_distance_intra_cluster(samples, labels):
    """
    Calculate the average distance between each pair of samples belonging
    to the same cluster
    """

    log("Pairwise distance intracluster DEBUG", parameters.LOG_SOFT_DEBUG)
    # log("SAMPLES")
    # log(samples)

    # log("LABELS")
    # log(labels)

    unique_labels = set(labels)
    number_of_samples = len(labels)
    distances_per_cluster = []
    stdev_per_cluster = []
    variance_per_cluster = []

    for label in unique_labels:
        samples_in_cluster = []
        for i in range(0, number_of_samples):
            if labels[i] == label:
                samples_in_cluster.append(samples[i])

        # An array where D[i,j] corresponds to the distance from sample i to j
        distances_array = pairwise_distances(samples_in_cluster, metric="euclidean")

        # Distance, Stdev and Variance inside cluster
        distances_per_cluster.append(np.mean(distances_array))
        stdev_per_cluster.append(np.std(distances_array))
        variance_per_cluster.append(np.var(distances_array))

    # log("Distances per cluster (averages?)")
    # log(distances_per_cluster)


    result = {
        "distance": (distances_per_cluster),
        "stdev":  (stdev_per_cluster),
        "variance": (variance_per_cluster)
    }

    # log("result")
    # log(result)

    return result

def pairwise_distance_between_centroids(centroids):
    """
    Calculate the average distance between centroids
    """

    average_distance = np.mean(pairwise_distances(centroids, metric="euclidean"))

    return average_distance

def average_centroid_distance(samples, labels, centroids):
    """
    Calculate the cluster inertia: the sum of squared distance for each point
    to its closest centroid
    """

    unique_labels = set(labels)
    number_of_samples = len(labels)
    distances_per_cluster = []

    for label in unique_labels:
        samples_in_cluster = []
        for i in range(0, number_of_samples):
            if labels[i] == label:
                samples_in_cluster.append(samples[i])

        centroid = [centroids[label]]
        distances_per_cluster.append(np.mean(pairwise_distances(samples_in_cluster, centroid, metric="euclidean")))

    average_distance = np.mean(distances_per_cluster)

    return average_distance
