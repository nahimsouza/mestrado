import parameters
import matplotlib.pyplot as plt


def plot_2d_points(samples, color=parameters.COLOR_RUSTY_RED, marker=parameters.MARKER_CIRCLE, title="", save_name=""):
    """
    This function must plot the 2D samples on a image
    """
    plt.plot(*zip(*samples), marker=marker, color=color, linestyle='', markeredgecolor="#000000", markeredgewidth="0.7",)

    plt.title(title)

    plt.xlim([0,1])
    plt.ylim([0,1])

    if parameters.PLOT_SAVE_FIG == True:
        plt.savefig(parameters.PLOT_CURRENT_DIR + save_name)
        plt.clf()
    else:
        plt.show()


    return
