import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.datasets import make_blobs
from sklearn.datasets import make_gaussian_quantiles

import numpy as np

# plt.figure(figsize=(8, 8))
# plt.subplots_adjust(bottom=.05, top=.9, left=.05, right=.95)


# colocar aqui os 6 plots das bases de dados artificiais
# salvar cada um deles em arquivos
# depois, rodar os classificadores para essas bases

# plt.subplot(321)
plt.title("Two blobs, four classes", fontsize='small')
X1, Y1 = make_blobs(n_samples=600, n_features=2, centers=[[-2,-2],[-2,1],[3,-2],[3,1]], cluster_std=1.2)
# X1, Y1 = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=1, n_clusters_per_class=1)
print("DATA 1: ")
for i in range(len(Y1)):
    if Y1[i]==2:
        Y1[i]=1

    if Y1[i]==3:
        Y1[i]=0
print(X1)
print(Y1)
plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)

samples = []
for i in range(0,len(Y1)):
    samples.append(list(X1[i]))
    samples[i].append(Y1[i])

# print(samples)
encoded_file_path = "../resources/original_data/2blobs-inv-2.csv"
np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# --------------------------------------------------------------------

# plt.subplot(322)
# plt.title("Two blobs, four classes - mixed", fontsize='small')
# X1, Y1 = make_blobs(n_samples=500, n_features=2, centers=[[-4,-4],[-3,-4],[4,4],[4,3]], cluster_std=1.5)
# # X1, Y1 = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, n_clusters_per_class=1)
# print("DATA 2: ")
# print(X1)
# print(Y1)
# plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)


# samples = []
# for i in range(0,len(Y1)):
#     samples.append(list(X1[i]))
#     samples[i].append(Y1[i])

# # print(samples)
# encoded_file_path = "../resources/original_data/2blobs4classes.csv"
#np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# --------------------------------------------------------------------

# plt.subplot(322)
# plt.title("4 blobs, 3 classes - mixed", fontsize='small')
# X1, Y1 = make_blobs(n_samples=300, n_features=2, centers=[[-3,-3],[-2,1],[2,2],[2,-3]], cluster_std=0.8)
# for i in range(len(Y1)):
#     if Y1[i]==3:
#         Y1[i]=1

#     if Y1[i]==2:
#         Y1[i]=0
# print("DATA 2: ")
# print(X1)
# print(Y1)
# plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)


# samples = []
# for i in range(0,len(Y1)):
#     samples.append(list(X1[i]))
#     samples[i].append(Y1[i])

# # print(samples)
# encoded_file_path = "../resources/original_data/4bl2cl-small.csv"
# np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# # --------------------------------------------------------------------


# plt.subplot(323)
# plt.title("Four clusters with intersection between two", fontsize='small')
# X1, Y1 = make_blobs(n_samples=500, n_features=2, centers=[[-4,0],[0,-5],[0,5],[4,0]], cluster_std=1.5)
# # X2, Y2 = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2)
# print("DATA 3: ")
# print(X1)
# print(Y1)
# plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)


# samples = []
# for i in range(0,len(Y1)):
#     samples.append(list(X1[i]))
#     samples[i].append(Y1[i])

# # print(samples)
# encoded_file_path = "../resources/original_data/4clusters.csv"
# #np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# # --------------------------------------------------------------------


# plt.subplot(324)
# plt.title("Three blobs", fontsize='small')
# X1, Y1 = make_blobs(n_samples=500, n_features=2, centers=[[-4,-4],[-4,1],[6,8]], cluster_std=1.8)
# # X1, Y1 = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, n_clusters_per_class=1, n_classes=3)
# print("DATA 4: ")
# print(X1)
# print(Y1)
# plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)


# samples = []
# for i in range(0,len(Y1)):
#     samples.append(list(X1[i]))
#     samples[i].append(Y1[i])

# # print(samples)
# encoded_file_path = "../resources/original_data/3blobs.csv"
# #np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# # --------------------------------------------------------------------


# plt.subplot(325)
# plt.title("Four blobs, Three classes", fontsize='small')
# X1, Y1 = make_blobs(n_samples=500, n_features=2, centers=[[-6,-0],[-2,1],[2,2],[5,1]], cluster_std=1.2)
# for i in range(len(Y1)):
#     if Y1[i]==2:
#         Y1[i]=1

#     if Y1[i]==3:
#         Y1[i]=2
# print("DATA 5: ")
# print(X1)
# print(Y1)


# plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)


# samples = []
# for i in range(0,len(Y1)):
#     samples.append(list(X1[i]))
#     samples[i].append(Y1[i])

# # print(samples)
# encoded_file_path = "../resources/original_data/4blobs3classes.csv"
# #np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# # --------------------------------------------------------------------


# plt.subplot(326)
# plt.title("Gaussian divided into three quantiles", fontsize='small')
# X1, Y1 = make_gaussian_quantiles(n_samples=500, n_features=2, n_classes=4)
# print("DATA 6: ")
# print(X1)
# print(Y1)
# plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1)


# samples = []
# for i in range(0,len(Y1)):
#     samples.append(list(X1[i]))
#     samples[i].append(Y1[i])

# # print(samples)
# encoded_file_path = "../resources/original_data/gaussian-4classes.csv"
# #np.savetxt(encoded_file_path, np.array(samples, dtype=float), delimiter=",", fmt="%.2f")

# # --------------------------------------------------------------------


plt.show()