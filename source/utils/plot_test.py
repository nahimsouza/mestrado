#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
=====================
Classifier comparison
=====================

A comparison of a several classifiers in scikit-learn on synthetic datasets.
The point of this example is to illustrate the nature of decision boundaries
of different classifiers.
This should be taken with a grain of salt, as the intuition conveyed by
these examples does not necessarily carry over to real datasets.

Particularly in high-dimensional spaces, data can more easily be separated
linearly and the simplicity of classifiers such as naive Bayes and linear SVMs
might lead to better generalization than is achieved by other classifiers.

The plots show training points in solid colors and testing points
semi-transparent. The lower right shows the classification accuracy on the test
set.
"""
#print(__doc__)


# Code source: Gaël Varoquaux
#              Andreas Müller
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.datasets import make_moons, make_circles, make_classification, make_blobs
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

from sklearn.cluster import KMeans
import csv

h = .02  # step size in the mesh

names = ["KNN", "Radial SVM", 
        #"Decision Tree",
         "Naive Bayes", 
         "LDA"
         ]

classifiers = [
    KNeighborsClassifier(3),
    SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False),
    #DecisionTreeClassifier(max_depth=5),
    GaussianNB(),
    LinearDiscriminantAnalysis()
    ]


def prepare_ds():
    y = []

    data = csv.reader(open('../resources/original_data/2globs.txt'), delimiter='\t')
    bX = []
    for r in data:
        bX.append([float(r[1]),float(r[2])])
        y.append(int(r[3]))


    g1c0 = 0
    g1c1 = 0
    g2c0 = 0
    g2c1 = 0

    # workaround to set positions in y:
    for i in range(0,len(y)):
        if (i > 261):
            if (bX[i][1] < 30.3):
                y[i] = 1
                g1c0 += 1
            else:
                y[i] = 0
                g1c1 += 1 
        else:
            if (bX[i][0] < 21   ):
                g2c0 += 1
                y[i] = 1
            else:
                g2c1 += 1
                y[i] = 0

    print g1c0, g1c1, g2c0, g2c1


    blob_data = (bX, y)
    datasets = [blob_data]
    
    return datasets


def ploting(k, idx, datasets):

    pred = []

    figure = plt.figure(figsize=(16, 8))

    i = 1
    # iterate over datasets
    for ds in datasets:

        # preprocess dataset, split into training and test part
        X, y = ds
        X = MinMaxScaler().fit_transform(X)

        # TEST_----------------------------

        kmeans = KMeans(init='k-means++', n_clusters=k, n_init=30, n_jobs=2)
        kmeans.fit(X)

        labels = kmeans.labels_
        # print labels

        count = 0
        clustered_X = []
        clustered_Y = []
        for sample in X:
            if labels[count] == idx:
                clustered_X.append([sample[0],sample[1]])
                clustered_Y.append(y[count])
            count+=1

        # ENDTEST_ ------------------------

        clustered_X = np.vstack(clustered_X)


        ### CLASSIFICATION ####
        X_train, X_test, y_train, y_test = train_test_split(clustered_X, clustered_Y, test_size=.20)

        x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
        y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))

        

        # just plot the dataset first
        #cm = plt.cm.RdBu
        #cm = plt.cm.RdBu
        cm = ListedColormap(['#00FF00', '#0000FF'])
        cm_bright = ListedColormap(['#00FF00', '#0000FF'])

        ax = plt.subplot(len(datasets), len(classifiers) + 1, i)
        
        # Plot the training points
        #ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright)
        
        # and testing points
        ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright)
        #ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)
        
        ax.set_xlim(xx.min(), xx.max())
        ax.set_ylim(yy.min(), yy.max())
        ax.set_xticks(())
        ax.set_yticks(())
        i += 1

        if k > 1:
            Zkmeans = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

            # Put the result into a color plot
            Zkmeans = Zkmeans.reshape(xx.shape)

            #plt.contour(xx, yy, Zkmeans, zorder=20, colors='#FF0000', contours=1)
            # Plot the centroids as a white X
            centroids = kmeans.cluster_centers_
            plt.scatter(centroids[:, 0], centroids[:, 1],
                        marker='x', s=100, linewidths=3,
                        color='r', zorder=20)

        # -------------------------------------------------

        print "\n" 

        # iterate over classifiers
        for name, clf in zip(names, classifiers):
            ax = plt.subplot(len(datasets), len(classifiers) + 1, i)
           
            clf.fit(X_train, y_train)
            pred = clf.predict(X_test)
            score = clf.score(X_test, y_test)

            # Plot the decision boundary. For that, we will assign a color to each
            # point in the mesh [x_min, m_max]x[y_min, y_max].
            if hasattr(clf, "decision_function"):
                Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
            else:
                Z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]

            #if k > 1:
                # Plot clustering:
                #ax.contour(xx, yy, Zkmeans, zorder=20, colors='#FF0000', contours=1)

            # Put the result into a color plot
            Z = Z.reshape(xx.shape)
            ax.contourf(xx, yy, Z, cmap=cm, alpha=.3)

            # Plot also the training points
            #ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright)
                
            # and testing points
            ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright)
            #ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)

            ax.set_xlim(xx.min(), xx.max())
            ax.set_ylim(yy.min(), yy.max())
            ax.set_xticks(())
            ax.set_yticks(())
            ax.set_title(name)
            #ax.text(xx.max() - .3, yy.min() + .3, ('%.2f' % score).lstrip('0'),
            #        size=15, horizontalalignment='right')

            print "Name: " + name + " Score: " +  str(score)

            i += 1

    figure.subplots_adjust(left=.02, right=.98)
    return pred, y_test


    # END FUNCTION --------------------------------------------------------


# ploting experiments:

ds = prepare_ds()

pred_, y_ = ploting(k=1, idx=0, datasets=ds)
pred_, y_ = ploting(k=2, idx=0, datasets=ds)
pred_, y_ = ploting(k=2, idx=1, datasets=ds)

# ploting(k=3, idx=0, datasets=ds)
# ploting(k=3, idx=1, datasets=ds)
# ploting(k=3, idx=2, datasets=ds)



plt.show()
