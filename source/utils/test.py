"""
This file contains general tests to validate classifiers
"""

import sys
sys.path.insert(0,'..')  # this include the parent folder files on path

import parameters

import machine_learning_wrapper as mlw
import format_data as fmt
import experiment as exp
import csv
import os

import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler
from matplotlib.colors import ListedColormap

import numpy as np
from sklearn.metrics import f1_score

from imblearn.over_sampling import SMOTE
from imblearn.combine import SMOTEENN
from imblearn.combine import SMOTETomek

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans

from sklearn.datasets.samples_generator import make_blobs
import random

from sklearn.datasets import load_iris
from sklearn.decomposition import PCA

from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder



DATASET = parameters.DATASETS



def is_balanced(labels):
    total_of_elements = len(labels)

    result = True

    perc_list = []

    print("Support/Balance per class")
    for label in set(labels):
        support = list(labels).count(label)
        percentage = (support/total_of_elements)*100

        perc_list.append(percentage)

        warn = ""
        if percentage < 5:
            warn = " - UNBALANCED"
            result = False

        print("Class: %d \t %d/%d \t %.2f%% %s"
             % (label, support, total_of_elements, percentage, warn))

    print("STDEV: %.3f" % np.std(perc_list))
    print("VAR: %.3f" % np.var(perc_list))

    return result

def test_smote():
    # for ds in sorted(DATASET):

    ds = parameters.DATASETS["2globs"]
    # print("\nDATASET: " + ds)
    json_data = exp.load_dataset(ds)

    X = json_data["training_set"]["features"]
    y = json_data["training_set"]["labels"]

    X = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
    y = [0,0,0,0,2,2,2,2,2,1,1,1,1,1]  # minor class needs to have at least 6 samples


    # Apply SMOTE
    sm = SMOTETomek()
    X_resampled, y_resampled = sm.fit_sample(X, y)

    is_balanced(y)
    is_balanced(y_resampled)


        # knn = mlw.train_classifier("KNN", X_resampled, y_resampled)
        # svm = mlw.train_classifier("LinearSVM", X_resampled, y_resampled)
        # dt = mlw.train_classifier("DecisionTree", X_resampled, y_resampled)

        # X_test = json_data["test_set"]["features"]
        # y_test = json_data["test_set"]["labels"]

        # pred_knn = mlw.classify(knn['clf'], X_test)
        # pred_svm = mlw.classify(svm['clf'], X_test)
        # pred_dt = mlw.classify(dt['clf'], X_test)

        # print("\nPairwise TRAINING")
        # mlw.pairwise_distance_intra_cluster(X,y)

        # print("\nPairwise TEST")
        # mlw.pairwise_distance_intra_cluster(X_test,y_test)

        # print("\nPerformance")
        # print("knn \tscore: " + str("%.2f" % knn['score']) + " \tfmacro: " + str("%.2f" % f1_score(y_test, pred_knn, average="macro")))
        # print("svm \tscore: " + str("%.2f" % svm['score']) + " \tfmacro: " + str("%.2f" % f1_score(y_test, pred_svm, average="macro")))
        # print("dt \tscore: " + str("%.2f" % dt['score']) + " \tfmacro: " + str("%.2f" % f1_score(y_test, pred_dt, average="macro")))

def get_best_k_using_gap(data, nrefs=3, max_clusters=15):
    """
    Calculates KMeans optimal K using Gap Statistic from Tibshirani, Walther, Hastie
    Params:
        data: ndarry of shape (n_samples, n_features)
        nrefs: number of sample reference datasets to create
        max_clusters: Maximum number of clusters to test for
    Returns: (optimalK, gaps_dataframe)
    """
    gaps = np.zeros((len(range(1, max_clusters)),))
    inertias = np.zeros((len(range(1, max_clusters)),))
    diffs = np.zeros((len(range(1, max_clusters)),))
    perc_diffs = np.zeros((len(range(1, max_clusters)),))

    resultsdf = pd.DataFrame({'clusterCount':[], 'gap':[]})
    for gap_index, k in enumerate(range(1, max_clusters)):

        # Holder for reference dispersion results
        refDisps = np.zeros(nrefs)

        # For n references, generate random sample and perform kmeans getting resulting dispersion of each loop
        for i in range(nrefs):

            # Create new random reference set
            randomReference = np.random.random_sample(size=data.shape)

            # Fit to it
            km = KMeans(k)
            km.fit(randomReference)

            refDisp = km.inertia_
            refDisps[i] = refDisp

        # Fit cluster to original data and create dispersion
        km = KMeans(k)
        km.fit(data)

        origDisp = km.inertia_

        # Calculate gap statistic
        gap = np.log(np.mean(refDisps)) - np.log(origDisp)

        # Assign this loop's gap statistic to gaps
        gaps[gap_index] = gap
        inertias[gap_index] = origDisp

        if gap_index > 0:
            diffs[gap_index] = abs(origDisp - inertias[gap_index-1])
            perc_diffs[gap_index] = (diffs[gap_index]/inertias[gap_index-1])*100
        else:
            diffs[gap_index] = origDisp
            perc_diffs[gap_index] = 100

        resultsdf = resultsdf.append({'clusterCount':k, 'gap':gap, 'inertia': origDisp,
                    'diffs':diffs[gap_index], 'perc_diffs':perc_diffs[gap_index]}, ignore_index=True)

    best_k = gaps.argmax() + 1 # Plus 1 because index of 0 means 1 cluster is optimal, index 2 = 3 clusters are optimal
    return best_k


def get_best_k_using_elbow(data, max_clusters):
    """
    Gets the best K using elbow method
    """

    inertias = np.zeros((len(range(1, max_clusters)),))
    diffs = np.zeros((len(range(1, max_clusters)),))
    perc_diffs = np.zeros((len(range(1, max_clusters)),))
    best_k = 1
    resultsdf = pd.DataFrame({'clusterCount':[], 'inertia':[], 'diffs':[], 'perc_diffs':[]})

    for index, k in enumerate(range(1, max_clusters)):

        # Fit cluster to original data and create dispersion
        km = KMeans(k)
        km.fit(data)

        inertias[index] = km.inertia_

        if index > 0:
            diffs[index] = abs(km.inertia_ - inertias[index-1])
            perc_diffs[index] = (diffs[index]/inertias[index-1])*100
        else:
            diffs[index] = km.inertia_
            perc_diffs[index] = 100

        # just for debbugging
        resultsdf = resultsdf.append({'clusterCount':k, 'inertia': km.inertia_,
                    'diffs':diffs[index], 'perc_diffs':perc_diffs[index]}, ignore_index=True)

        best_k = k
        if perc_diffs[index] < parameters.ELBOW_PERCENTAGE:
            break

    log("Best K = " + str(best_k), parameters.LOG_SOFT_DEBUG)
    return best_k


def get_optimal_k(data, max_clusters=15):

    result = None

    if parameters.USE_GAP_STATISTICS:
        result = get_best_k_using_gap(data, 3, max_clusters)
    else:
        result = get_best_k_using_elbow(data, max_clusters)

    return result

def test_gap():
    dataset = DATASET["wine"]
    json_data = exp.load_dataset(dataset)

    # gets [0] index that is the first fold
    X = json_data[0]["training_set"]["features"]
    y = json_data[0]["training_set"]["labels"]

    # print("X SEM PCA")
    # print(X)

    # cluster and find the best K
    number_of_samples = len(X)
    max_num_clusters = int(number_of_samples / parameters.MIN_ELEMENTS_PER_CLUSTER)
    max_num_clusters_by_percent = int(number_of_samples * parameters.MIN_ELEMENTS_PER_CLUSTER_PERCENT)

    # define the maximum number of clusters
    max_clusters = max_num_clusters_by_percent if max_num_clusters_by_percent > parameters.MIN_ELEMENTS_PER_CLUSTER else max_num_clusters


    result = get_optimal_k(X, max_clusters)

    print("\n\n GAP RESULT \n" + str(result))
    print("END")

    return

def test_cluster_by_class():
    for name in parameters.DATASETS_LIST:
        try:
            print("\n>>>> Dataset: " + name)
            dataset = parameters.DATASETS[name]
            folds = exp.load_dataset(dataset)
            count = 0
            for fold in folds:
                count += 1
                print("\nFold " + str(count))
                st = os.times()
                X = fold["training_set"]["features"]
                y = fold["training_set"]["labels"]
                result = mlw.cluster_by_class(X, y)
                et = os.times() # the tuple with fields: (utime, stime, cutime, cstime, elapsed_time)
                # sys + user = total cpu time
                total_time = (et[0]-st[0]) + (et[1]-st[1])
                print("\n> Cpu Time for cluster_by_class fold: %.2fs\n" % (total_time))
        except Exception as e:
            print("Exception on dataset: " + name + " - skipped")
            print("Exception message: " + str(e))

    return

def get_sts_n_random_samples(n = 16000):

    index = list(range(1600000)) # sts has 1.6M samples
    random.shuffle(index)

    selected_samples = index[0:n] # get 10k samples shuffled

    data = csv.reader(open("../../resources/original_data/sts-full.csv"), delimiter=",")

    file = open("../../resources/original_data/sts16k.csv", "w")

    count = 0
    for row in data:
        if count in selected_samples:
            file.write("\"" + str(row[0]) + "\", \"" + str(row[5]) + "\"\n")
        count += 1

    file.close()
    print("Done")

def test_kfold():
    print(fmt.generate_kfold("iris.data.json", 5))

def preprocessing_mushroom():
    file = csv.reader(open("../../resources/original_data/mushrooms.csv"), delimiter=",")

    samples = []

    for row in file:
        samples.append(row)

    # convert to numpy array
    samples = np.array(samples)
    encoded = np.zeros(samples.shape) # create a temp array to put encoded features

    # encode label (label and features are categorical)
    label_encoder = LabelEncoder().fit(samples[:,0])
    encoded[:,0] = label_encoder.transform(samples[:,0])

    # encode columns
    for i in range(1, len(samples[0])):
        label_encoder = LabelEncoder().fit(samples[:,i])
        encoded[:,i] = label_encoder.transform(samples[:,i])


    encoder = preprocessing.OneHotEncoder().fit(encoded[:,1:])  # get all lines, but discard the label column

    encoded_samples = []

    count = 0
    for row in encoded:
        encoded_samples.append([int(row[0])])

        # Needs to reshape because of sklearn warning:
        # >> Passing 1d arrays as data is deprecated in 0.17 and will raise ValueError in 0.19.
        #    Reshape your data either using X.reshape(-1, 1) if your data has a single feature
        #    or X.reshape(1, -1) if it contains a single sample.

        transformed_features = encoder.transform(row[1:].reshape(1,-1)).toarray().astype(int)
        # print(transformed_features)
        encoded_samples[count].extend(transformed_features[0])

        count += 1
    # encoded[0][1:] = np.zeros((1,22))
    print(encoded[0])
    print(encoded[8123])

    print(encoded_samples[0])
    print(encoded_samples[8123])

    encoded_file_path = "../../resources/original_data/mushrooms-encoded.csv"
    np.savetxt(encoded_file_path, encoded_samples, delimiter=",", fmt="%d")

def preprocess_madelon():

    # get samples features
    file = csv.reader(open("../resources/original_data/madelon/madelon_train.data.txt"), delimiter=" ")
    samples = []
    for row in file:
        samples.append(row)

    # get samples labels
    file = csv.reader(open("../resources/original_data/madelon/madelon_train.labels.txt"), delimiter=" ")
    labels = []
    for row in file:
        labels.append(row)

    # merge samples and labels
    for i in range(0,len(labels)):
        samples[i].extend(labels[i])


    print(samples)

    encoded_file_path = "../resources/original_data/madelon-full.csv"
    np.savetxt(encoded_file_path, np.array(samples,dtype=int), delimiter=",", fmt="%d")

    return 


def preprocess_activity():

    # get samples features
    file = csv.reader(open("../resources/original_data/activity-recognition/train/X_train.txt"), delimiter=" ")
    samples = []
    for row in file:
        samples.append(row)

    # get samples labels
    file = csv.reader(open("../resources/original_data/activity-recognition/train/y_train.txt"), delimiter=" ")
    labels = []
    for row in file:
        labels.append(row)

    # merge samples and labels
    for i in range(0,len(labels)):
        samples[i].extend(labels[i])


    print(samples)

    encoded_file_path = "../resources/original_data/activity-recognition-full.csv"
    np.savetxt(encoded_file_path, np.array(samples,dtype=float), delimiter=",", fmt="%f")

    return 




def preprocess_parkinson():

    # get samples features
    # the class 'status' is in the middle of features
    file = csv.reader(open("../resources/original_data/parkinson/parkinsons.data.txt"), delimiter=",")
    samples = []
    count = 0
    for row in file:
        samples.append(row[1:16])
        samples[count].extend(row[18:])
        samples[count].append(row[17])
        count += 1

    print(samples)

    encoded_file_path = "../resources/original_data/parkinson-full.csv"
    np.savetxt(encoded_file_path, np.array(samples,dtype=float), delimiter=",", fmt="%f")

    return 


def test_plot():
    ds = parameters.DATASETS["2blobs4classes"]
    json_data = exp.load_dataset(ds)

    X = json_data["training_set"]["features"]
    y = json_data["training_set"]["labels"]

    # scale 
    X = MinMaxScaler().fit_transform(X)

    # clf = KNeighborsClassifier()
    clf = LogisticRegression()    
    clf.fit(X,y)

    h = 0.002 # step size in the mesh
    x_min, x_max = X[:, 0].min(), X[:, 0].max()
    y_min, y_max = X[:, 1].min(), X[:, 1].max()
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    
    print(xx.shape)
    print(Z)
    print(len(Z))

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)

    plt.contourf(xx, yy, Z, cmap=plt.cm.YlGnBu , alpha=.9)
    
    plt.plot(*zip(*X), marker="o", color="#DDDDDD", linestyle='')

    plt.title("TITLE HERE")

    plt.xlim([0,1])
    plt.ylim([0,1])

    plt.savefig("test.png")

    plt.show()



if __name__ == '__main__':
    test_plot()