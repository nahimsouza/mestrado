"""
===========
Main File
===========

This file contains the main function that calls all experiments
"""

import experiment
import parameters
from logger import log

import os
import shutil

if parameters.LOG_LEVEL != parameters.LOG_FULL_DEBUG:
    # Magic code to ignore scikit-learn warnings when not deugging
    def warn(*args, **kwargs):
        pass
    import warnings
    warnings.warn = warn

def main():

    # Ensure plot folder will be empty
    if parameters.PLOT_2D_DATA == True:
        if os.path.exists("./plot/"):
            shutil.rmtree("./plot/")

        parameters.PLOT_CURRENT_DIR = "./plot/"

    # Run Experiments:
    for key in parameters.DATASETS_LIST:
        
        # adding key to use as label on results
        parameters.DATASETS[key]["key"] = key

        try:
            experiment.run(parameters.DATASETS[key])
        except Exception as e:
            log("Exception on dataset: " + key + " - skipped", parameters.LOG_ERROR)
            
            if parameters.LOG_LEVEL == parameters.LOG_FULL_DEBUG:
                log("Traceback: " + traceback.format_exc())
                log("Exception message: " + str(e), parameters.LOG_ERROR)

if __name__ == '__main__':
    main()