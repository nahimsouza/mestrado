"""
===========
Experiment
===========

This file contains the functions and variables corresponding to one single
experiment.

"""

from sklearn import metrics

import scipy.spatial.distance as distance
import numpy as np

import format_data as fmt
import machine_learning_wrapper as mlw

from statistics import mean, stdev

# from plotly.offline import plot
# from plotly.tools import FigureFactory as FF
# import plotly.graph_objs as go

from imblearn.over_sampling import SMOTE
from imblearn.combine import SMOTEENN
from imblearn.combine import SMOTETomek

import copy
import os
import shutil
import parameters

# import plot_wrapper as plw
from logger import log

if parameters.PLOT_2D_DATA:
    import matplotlib.pyplot as plt
    import plot_wrapper as plw
    from matplotlib.colors import ListedColormap    

# GLOBAL AUX VARIABLE
centroids_ = None


def load_dataset(params):
    """
    Loads the dataset in a json format and returns the data partitioned in
    three subsets: Training Set, Selection Set and Test Set.

    Args:
        params (dict): The parameters do load the dataset: dataset name,
                           columns of labels and features and the delimiter used
                           to separate the data.

    Returns:
        dict: The partitoned data in three subsets.

    """

    log("load_datset params: " + str(params), parameters.LOG_FULL_DEBUG)

    json_dataset = fmt.load_dataset(
                        dataset = params["dataset"],
                        labels_col = params["labels_col"],
                        features_first_col = params["features_first_col"],
                        features_last_col = params["features_last_col"],
                        delimiter = params["delimiter"])

    if parameters.USE_KFOLD:
        partitioned_data = fmt.generate_kfold(dataset = json_dataset,
                                              number_of_folds = parameters.NUMBER_OF_FOLDS,
                                              scale=parameters.NORMALIZE_DATA)
    else:
        partitioned_data = fmt.generate_subsets(dataset = json_dataset,
                                                training_size = 80,
                                                selection_size = 0,
                                                test_size = 20,
                                                data_scale=parameters.NORMALIZE_DATA)

    return partitioned_data

def generate_partitions(data, begin=2, end=10):
    """
    Generates partitions for dataset.
    """
    clusters = []

    # always consider a single cluster for classification comparison
    # if (begin != 1):
    #     clusters.append(mlw.cluster(number_of_clusters=1, features=data))

    i = end
    while i >= begin:
        cluster = mlw.cluster(number_of_clusters=i, features=data)

        # It means that one or more clusters had too few elements and were merged
        if cluster["number_of_clusters"] < i:
            log("WARNING: Number of clusters were reduced because one or more clusters had too few elements", parameters.LOG_WARNING)
            log("WARNING: The maximum value of K was " +  str(cluster["number_of_clusters"]), parameters.LOG_WARNING)
            clusters.append(cluster)
            i = cluster["number_of_clusters"]
        else:
            clusters.append(cluster)
        i = i - 1

    return clusters

def get_majority_label(labels):
    total_of_elements = len(labels)
    major = labels[0]

    for label in set(labels):
        counter = list(labels).count(label)
        if (counter > list(labels).count(major)):
            major = label

    return major

def is_balanced(labels):
    total_of_elements = len(labels)
    percentage_list = []
    result = True

    # log("Support/Balance per class")
    for label in set(labels):
        support = list(labels).count(label)
        percentage = (support/total_of_elements)*100
        percentage_list.append(percentage)
        # log("Class: %d \t %d/%d \t %.2f%% %s"
        #      % (label, support, total_of_elements, percentage, warn))

    stdev_percentage = np.std(percentage_list)
    if stdev_percentage > parameters.BALANCE_STDEV_THRESHOLD:
        result = False

    # log("STDEV: %.3f" % np.std(perc_list))
    # log("VAR: %.3f" % np.var(perc_list))

    return result

def all_equal(items):
    return all(x == items[0] for x in items)

def generate_pool_for_one_partition(data, labels):

    """
    It generates the pool for only one partition, that is
    obtained using cluster-by-class method for clustering.

    """

    start_time = os.times()

    global centroids_
    
    if centroids_ != None:
        cluster_by_class = mlw.cluster_by_centroid(data, labels, centroids_)
    else:
        cluster_by_class = mlw.cluster_by_class(data, labels)
    #################

    log("---- CLUSTER BY CLASS -----", parameters.LOG_FULL_DEBUG)
    log(cluster_by_class, parameters.LOG_FULL_DEBUG)

    centroids = []

    partition = cluster_by_class["partition"]

    pool_of_classifiers = []

    # kept for compatibility with old version
    pool_iteration = {
        "k": cluster_by_class["number_of_clusters"],
        # "silhouette": cluster["silhouette"],
        "average_centroid_distance": cluster_by_class["average_centroid_distance"],
        "pairwise_centroid_distance": cluster_by_class["pairwise_distance_between_centroids"],
        "pairwise_intracluster_distance": cluster_by_class["pairwise_distance_intra_cluster"],
        "pairwise_intracluster_distance_stdev": cluster_by_class["pairwise_distance_intra_cluster_stdev"],
        "pairwise_intracluster_distance_variance": cluster_by_class["pairwise_distance_intra_cluster_variance"],
        "trained":[]
    }

    log("FINAL PARTITION SIZE: " + str(len(partition)), parameters.LOG_SOFT_DEBUG)


    # Trains classifier per group of Final Partition
    for cluster_id in partition:

        clf_start_time = os.times()

        # Adds the centroid in a list to improve performance later
        centroids.append(partition[cluster_id]["centroid"])

        # Group the samples and labels belonging to the same cluster
        clustered_samples = partition[cluster_id]["samples"]
        clustered_labels = partition[cluster_id]["labels"]
        classification_result = {}

        # If it is not balanced, uses SMOTE+Tomek from Imbalanced-Learn library to balance
        smote_failed = False
        if parameters.USE_SMOTE and is_balanced(clustered_labels) == False:
            try:
                smote = SMOTETomek()
                clustered_samples, clustered_labels = smote.fit_sample(clustered_samples, clustered_labels)
                smote_failed = False
            except ValueError as err:
                smote_failed = True
                log("WARNING: SMOTE failed on cluster %s: %s" % (str(cluster_id), str(err)), parameters.LOG_WARNING)
                log("WARNING: Considering majority class because smote failed...", parameters.LOG_WARNING)

        classifiers = []
        if all_equal(clustered_labels) or smote_failed:
            # Uses a dummy classifier to classify considering the most frequent label
            trained_classifier = mlw.train_classifier(parameters.CLASSIFIER_DUMMY, clustered_samples, clustered_labels)
            classifiers.append(trained_classifier)
        else:
            # Trains the classifiers for specific cluster
            for method in parameters.CLASSIFIERS_LIST:
                trained_classifier = mlw.train_classifier(method, clustered_samples, clustered_labels)
                classifiers.append(trained_classifier)

        if parameters.PLOT_2D_DATA:
            # Just Ploting Intersections
            log("plotting final intersections....")
            separated = mlw.separate_samples_by_label(clustered_samples, clustered_labels)

            for clf in classifiers:
                for lbl in separated:
                    samps = separated[lbl]["samples"]                

                    # To plot the classifier
                    h = 0.002 # step size in the mesh
                    x_min, x_max = (0,1)
                    y_min, y_max = (0,1)
                    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
                    Z = clf["clf"].predict(np.c_[xx.ravel(), yy.ravel()])
                    
                    # Put the result into a color plot
                    Z = Z.reshape(xx.shape)
                    plt.contourf(xx, yy, Z, cmap=plt.cm.YlGnBu , alpha=.6)

                    plt.plot(*zip(*samps), 
                                marker=parameters.MARKER_LIST[int(lbl)], #markersize=7, 
                                markeredgecolor="#000000", markeredgewidth="0.7",
                                color=parameters.COLOR_PALETTE[int(lbl)], linestyle='')
                    plt.xlim([0,1])
                    plt.ylim([0,1])

                plt.title("Final Intersection #" + str(cluster_id) + "\nClf Method: " + clf["method"])
                if parameters.PLOT_SAVE_FIG == True:
                    plt.savefig(parameters.PLOT_CURRENT_DIR + "intersection_n" + str(cluster_id) + "-clf_" + clf["method"] + ".png")
                    plt.clf()
                else:
                    plt.show()


        # Adds the classifiers on pool
        classification_result = {
            "cluster_id": cluster_id,
            "cluster_centroid":partition[cluster_id]["centroid"],
            "classifiers": classifiers,
        }

        pool_iteration["trained"].append(classification_result)


        clf_end_time = os.times()

        # cpu_time = user + sys time
        log("CPU TIME - classifier_training_for_group: %.2f" % ((clf_end_time[0] - clf_start_time[0]) + (clf_end_time[1] - clf_start_time[1])), parameters.LOG_ALWAYS)

    pool_of_classifiers.append(pool_iteration)

    if centroids_ == None:
        centroids_ = centroids


        # plot final groups (only the first fold)
        if parameters.PLOT_2D_DATA:
            # To plot the classifier
            h = 0.002 # step size in the mesh
            x_min, x_max = (0,1)
            y_min, y_max = (0,1)
            xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
                
            # Z = km.predict(np.c_[xx.ravel(), yy.ravel()])

            xy_samples = np.c_[xx.ravel(), yy.ravel()]

            # get nearest centroid, to plot groups

            groups = get_nearest_centroid_index(xy_samples, centroids_)

            # ------------------------------------------

            Z = np.array(groups)

            # Put the result into a color plot
            Z = Z.reshape(xx.shape)
            plt.contourf(xx, yy, Z, cmap=plt.cm.YlGnBu, alpha=0.4)
            plt.contour(xx, yy, Z, cmap=ListedColormap(["#090909"]), linewidths=0.7, zorder=10)

            # Plot centroids
            # plt.scatter(centroids[:, 0], centroids[:, 1],
            #             marker='x', s=80, linewidths=10,
            #             color='w', zorder=10)

            samples_by_label = mlw.separate_samples_by_label(data, labels)

            for i in range(0,len(samples_by_label)):
                plt.plot(*zip(*samples_by_label[str(i)]["samples"]), 
                            marker=parameters.MARKER_LIST[i % len(parameters.MARKER_LIST)], 
                            markeredgecolor="#000000", markeredgewidth="0.7",
                            color=parameters.COLOR_PALETTE[i], linestyle='')

            plt.xlim([0,1])
            plt.ylim([0,1])

            plt.title("Full Groups")
            if parameters.PLOT_SAVE_FIG == True:
                plt.savefig(parameters.PLOT_CURRENT_DIR + "final_groups.png")
                plt.clf()
            else:
                plt.show()

        
    

    log("CENTROIDS >>>", level=parameters.LOG_FULL_DEBUG)
    log(centroids, level=parameters.LOG_FULL_DEBUG)

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - generate_pool_for_one_partition: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_ALWAYS)

    return pool_of_classifiers

def get_nearest_centroid_index(samples, centroids):

    nearest_centroids = []
    
    for sample in samples:
        distance_nearest = float("inf") # infinite
        index = 0
        for i, centroid in enumerate(centroids):
            distance_to_centroid = distance.euclidean(sample, centroid)

            if distance_to_centroid < distance_nearest:
                distance_nearest = distance_to_centroid
                index = i
        
        nearest_centroids.append(index)

    return nearest_centroids
            

def generate_pool(data, labels):
    """
    Generates the pool of classifiers.
    """

    partitions = generate_partitions(data, parameters.MIN_NUMBER_OF_CLUSTERS, parameters.MAX_NUMBER_OF_CLUSTERS)
    pool_of_classifiers = []

    for partition in partitions:
        pool_iteration = {
            "k": partition["number_of_clusters"],
            # "silhouette": cluster["silhouette"],
            "average_centroid_distance": partition["average_centroid_distance"],
            "pairwise_centroid_distance": partition["pairwise_distance_between_centroids"],
            "pairwise_intracluster_distance": partition["pairwise_distance_intra_cluster"],
            "pairwise_intracluster_distance_stdev": partition["pairwise_distance_intra_cluster_stdev"],
            "pairwise_intracluster_distance_variance": partition["pairwise_distance_intra_cluster_variance"],
            "trained":[]
        }

        for i in range(0, partition["number_of_clusters"]):
            # Get the indexes of samples belonging to the i-th cluster
            nth_cluster_indexes = [index for index, label in enumerate(partition["labels"]) if label == i]

            # Group the samples and labels belonging to the same cluster
            clustered_samples = [data[i] for i in nth_cluster_indexes]
            clustered_labels = [labels[i] for i in nth_cluster_indexes]

            classification_result = {}

            log("clustered_labels: " + str(clustered_labels))


            # If it is not balanced, uses SMOTE+Tomek from Imbalanced-Learn library to balance
            smote_failed = False
            if parameters.USE_SMOTE and is_balanced(clustered_labels) == False:
                try:
                    smote = SMOTETomek()
                    clustered_samples, clustered_labels = smote.fit_sample(clustered_samples, clustered_labels)
                    smote_failed = False
                except ValueError as err:
                    smote_failed = True
                    log("WARNING: SMOTE failed on cluster %s: %s" % (str(cluster_id), str(err)), parameters.LOG_WARNING)
                    log("WARNING: Considering majority class because smote failed...", parameters.LOG_WARNING)

            classifiers = []
            if all_equal(clustered_labels) or smote_failed:
                # Uses a dummy classifier to classify considering the most frequent label
                classifiers.append(mlw.train_classifier(parameters.CLASSIFIER_DUMMY, clustered_samples, clustered_labels))
            else:
                # Trains the classifiers for specific cluster
                for method in parameters.CLASSIFIERS_LIST:
                    classifiers.append(mlw.train_classifier(method, clustered_samples, clustered_labels))

            # Adds the classifiers on pool
            classification_result = {
                "cluster_id": cluster_id,
                "cluster_centroid":partition[cluster_id]["centroid"],
                "classifiers": classifiers,
            }

            pool_iteration["trained"].append(classification_result)

        pool_of_classifiers.append(pool_iteration)

    return pool_of_classifiers

def get_centroids(pool_iteration):
    """
    Get the centroids for one iteration over pool of classifiers.

    Returns: The centroids related to each cluster, as described below:

             centroids = [
                 {cluster_id: 1,  centroid: [x,y,z], nearest_samples: [(appended later)], labels:[]},
                 {cluster_id: 2,  centroid: [x,y,z], nearest_samples: [(appended later)], labels:[]},
                 {cluster_id: 3,  centroid: [x,y,z], nearest_samples: [(appended later)], labels:[]}
             ]
    """
    centroids = []
    for clf in pool_iteration["trained"]:
        centroids.append({
            "cluster_id": clf["cluster_id"],
            "centroid": clf["cluster_centroid"],
            "nearest_samples":[],
            "labels":[]
        })

    return centroids

def calculate_nearest_samples(pool_iteration, data, labels, centr=None):
    """
    Calculate the nearest samples from each centroid.

    Returns: An array with centroids related with each cluster and the nearest
             samples with labels, as described below:

             centroids = [
                 {cluster_id: 1,  centroid: [x,y,z], nearest_samples: [[x1,y1,z1],[x2,y2,z2]], labels:[0,1]},
                 {cluster_id: 2,  centroid: [x,y,z], nearest_samples: [...], labels:[...]},
                 {cluster_id: 3,  centroid: [x,y,z], nearest_samples: [...], labels:[...]}
             ]
    """

    # Get the centroids to calculate the nearest samples
    if(centr == None):
        centroids = get_centroids(pool_iteration)
    else:
        centroids = centr

    # For each sample in data, calculate the distance to each centroid
    for index in range(0, len(data)):
        sample = np.array(data[index]).astype(np.float)
        nearest = 0 # index of nearest centroid

        for i in range(1, len(centroids)):
            d_nearest = distance.euclidean(sample, centroids[nearest]["centroid"])
            d_current = distance.euclidean(sample, centroids[i]["centroid"])
            if (d_current < d_nearest):
                nearest = i

        centroids[nearest]["nearest_samples"].append(sample)
        centroids[nearest]["labels"].append(labels[index])

    return centroids

def get_partition_score(best_classifiers):
    """
    Receives a list with the best classifiers per cluster on partition
    and returns the total score of these classifiers on partition (weighted average)
    """

    partition_score = 0
    score_list = []
    number_of_samples_list = []

    for cluster in best_classifiers:
        number_of_samples_list.append(cluster["best_classifier"]["number_of_samples"])
        score_list.append(cluster["best_classifier"]["score"])

    total_of_samples = sum(number_of_samples_list)

    for i in range(0, len(best_classifiers)):
        partition_score += (number_of_samples_list[i] / total_of_samples) * score_list[i]

    return partition_score


def get_best_classifiers_on_pool(pool):
    """
    Get the bests classifiers for each cluster in each partition
    """

    start_time = os.times()

    best_classifiers = []

    for partition in pool:

        best_classifiers_on_partition = get_best_classifiers_on_partition(partition)
        partition_score = get_partition_score(best_classifiers_on_partition)

        partition_classifiers = {
            "best_classifiers_per_cluster": best_classifiers_on_partition,
            "partition_score": partition_score,
            "k": partition["k"]
        }

        best_classifiers.append(partition_classifiers)

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - get_best_classifiers_on_pool: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)

    return best_classifiers

def get_best_classifiers_on_partition(partition):
    """
    Return the best classifier for each cluster of partition

    Returns:
        An structure like this:
        [
        {
            "centroid": [[0,0,0],[1,1,-1]],
            "best_classifier": {
                     'method':'LinearSVM',
                     'clf':SVC(...),
                     'performance_on_selection':{},
                     'score':0.64814814814814814
                     'number_of_samples': 33
        },
        {
            "centroid": [[0,0,0],[1,1,-1]],
            "best_classifier": {
                'method':'LinearSVM',
                'clf':SVC(...),
                'performance_on_selection':{},
                'score':0.64814814814814814
                'number_of_samples': 353
            }
        }
        ]
    """

    start_time = os.times()

    best_classifiers_on_partition = []

    for cluster in partition["trained"]:
        cluster_classifier = {
            "centroid": cluster["cluster_centroid"],
            "best_classifier": get_best_classifier_on_cluster(cluster)
        }
        best_classifiers_on_partition.append(cluster_classifier)
    
    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - get_best_classifiers_on_partition: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)

    return best_classifiers_on_partition

def get_best_classifier_on_cluster(cluster):
    """
    Return the best classifier for the cluster
    """

    start_time = os.times()

    best_classifier = None
    for classifier in cluster["classifiers"]:
        if best_classifier == None:
            best_classifier = classifier
        elif best_classifier["score"] < classifier["score"]:
            best_classifier = classifier
    
    end_time = os.times()
    
    # cpu_time = user + sys time
    log("CPU TIME - get_best_classifier_on_cluster: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)
    
    return best_classifier


def get_nearest_centroid_classifier(partition, sample):
    """
    Find the nearest centroid and returns the related classifier with score
    """

    start_time = os.times()

    classifier = None
    distance_nearest = float("inf") # infinite

    for cluster in partition["best_classifiers_per_cluster"]:
        distance_to_centroid = distance.euclidean(sample, cluster["centroid"])

        if distance_nearest > distance_to_centroid:
            distance_nearest = distance_to_centroid
            classifier = cluster["best_classifier"]

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - get_nearest_centroid_classifier: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_FULL_DEBUG)

    return classifier

def select_best_classifier_for_sample(best_classifiers_on_pool, sample):
    """
    Select the best classifier for sample according to the nearest
    centroid and the major score

    TODO: e se o score empatar, pega o de menor K?
    """

    start_time = os.times()

    best_classifier = None

    log("get_best_classifiers_on_pool", parameters.LOG_FULL_DEBUG)
    log(str(best_classifiers_on_pool), parameters.LOG_FULL_DEBUG)

    for partition in best_classifiers_on_pool:
        classifier = get_nearest_centroid_classifier(partition, sample)

        log("CLASSIFIER: ", parameters.LOG_FULL_DEBUG)
        log(classifier, parameters.LOG_FULL_DEBUG)

        if best_classifier == None or classifier['score'] > best_classifier['score']:
            best_classifier = classifier

    trained_classifier = {
        "method": best_classifier['method'],
        "classifier": best_classifier['clf']
    }

    end_time = os.times()

    # cpu_time = user + sys time
    log("CPU TIME - select_best_classifier_for_sample: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_FULL_DEBUG)

    return trained_classifier

def select_best_classifier_per_score(pool, test_data, test_labels):
    """
    Selects the classifier according to the samples, classifies
    """

    start_time = os.times()

    best_classifiers = get_best_classifiers_on_pool(pool)
    predictions = []
    for sample in test_data:
        best = select_best_classifier_for_sample(best_classifiers, sample)

        # mlw.classify returns a list of elements, i.e. pred = ['1']
        # threfore we need to use 'extend' instead of 'append'
        predictions.extend(mlw.classify(best['classifier'], sample))

    if parameters.PLOT_2D_DATA:
        plot_final_classifiers(pool, test_data, test_labels)

    result = show_performance_report(test_labels, predictions, "SELECTION-PER-SCORE", parameters.LOG_SOFT_DEBUG)

    end_time = os.times()
    
    # cpu_time = user + sys time
    log("CPU TIME - select_best_classifier_per_score: %.2f" % ((end_time[0] - start_time[0]) + (end_time[1] - start_time[1])), parameters.LOG_SOFT_DEBUG)

    return result

def plot_final_classifiers(pool, samples, labels):

    # To plot the classifier
    h = 0.002 # step size in the mesh
    x_min, x_max = (0,1)
    y_min, y_max = (0,1)
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        
    # Z = km.predict(np.c_[xx.ravel(), yy.ravel()])

    xy_samples = np.c_[xx.ravel(), yy.ravel()]

    # ------------ Classification -------------

    best_classifiers = get_best_classifiers_on_pool(pool)
    predictions = []
    for sample in xy_samples:
        best = select_best_classifier_for_sample(best_classifiers, sample.tolist())

        # mlw.classify returns a list of elements, i.e. pred = ['1']
        # threfore we need to use 'extend' instead of 'append'
        predictions.extend(mlw.classify(best['classifier'], sample))

    # ------------------------------------------

    Z = np.array(predictions)

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.YlGnBu, alpha=0.4)
    plt.contour(xx, yy, Z, cmap=ListedColormap(["#090909"]), linewidths=0.7, zorder=10)

    # Plot centroids
    # plt.scatter(centroids[:, 0], centroids[:, 1],
    #             marker='x', s=80, linewidths=10,
    #             color='w', zorder=10)

    samples_by_label = mlw.separate_samples_by_label(samples, labels)

    for i in range(0,len(samples_by_label)):
        plt.plot(*zip(*samples_by_label[str(i)]["samples"]), 
                    marker=parameters.MARKER_LIST[i % len(parameters.MARKER_LIST)], 
                    markeredgecolor="#000000", markeredgewidth="0.7",
                    color=parameters.COLOR_PALETTE[i], linestyle='')

    plt.xlim([0,1])
    plt.ylim([0,1])

    plt.title("Full Intersection")
    if parameters.PLOT_SAVE_FIG == True:
        plt.savefig(parameters.PLOT_CURRENT_DIR + "full_intersection.png")
        plt.clf()
    else:
        plt.show()


def show_performance_report(labels, prediction, classifier, log_level=parameters.LOG_SOFT_DEBUG):
    """
    Evaluate the performance of classifier and show the metrics.
    """

    labels = list(np.array(labels, dtype=int))
    prediction = list(np.array(prediction, dtype=int))

    # Calculate metrics
    accuracy = metrics.accuracy_score(labels, prediction)
    precision = metrics.precision_score(labels, prediction, average='macro', pos_label=None)
    recall = metrics.recall_score(labels, prediction, average='macro', pos_label=None)
    f1_score_micro = metrics.f1_score(labels, prediction, average='micro', pos_label=None)
    f1_score_macro = metrics.f1_score(labels, prediction, average='macro', pos_label=None)

    support = []
    balance = []
    labels_list = list(set(labels))
    for label in labels_list:
        support.append(labels.count(label))
        balance.append(labels.count(label)/len(labels))

    # Show metrics
    log("\n------------------ Classifier Performance ------------------\n", log_level)

    log("Classifier: " + str(classifier) + "\n", log_level)

    for i in range(0, len(support)):
        log("Class: " + str(labels_list[i]) +
              "\t\tSupport: " + str(support[i]) +
              "\t\tBalance: " + str("%.2f" % balance[i]), parameters.LOG_FULL_DEBUG)


    log("\nAccuracy: " + str("%.2f" % accuracy) +
          "\t\tPrecision: " + str("%.2f" % precision) +
          "\t\tRecall: " + str("%.2f" % recall) +
          "\t\tF-micro: " + str("%.2f" % f1_score_micro) +
          "\t\tF-macro: " + str("%.2f" % f1_score_macro), log_level)

    result = {
        "accuracy": accuracy,
        "precision": precision,
        "recall": recall,
        "f1_score_micro": f1_score_micro,
        "f1_score_macro": f1_score_macro,
        "support": support,
        "balance": balance
    }

    return result

def show_metrics_average(performance_average, log_level=parameters.LOG_ALWAYS):
    """
    This method is to display the average performance of classifiers
    """


    log("\n================ Classifiers Performance ================\n", log_level)

    for method in performance_average:
        log("\n>> Classifier: " + str(method), log_level)
        for metric in performance_average[method]:
            if metric != "support" and metric != "balance":
                log("%s: %.2f" % (metric, np.mean(performance_average[method][metric])), log_level)
            else:  # if metric is support or balance
                metric_mean = []
                for i in range(0, len(performance_average[method][metric])):
                    #print("AVG METRIC = " + str(performance_average[method][metric]))
                    metric_mean = np.mean(performance_average[method][metric], axis=1)

                for i in range(0,len(metric_mean)):
                    log("Class: %d, %s: %.2f" % (i, metric, metric_mean[i]), parameters.LOG_FULL_DEBUG)

def get_best_results_array(best_results):
    """
    Transform the dictionary in an array with best results to calculate the
    average, variance and standard deviation

    Returns:
        The metrics dictionary with array of values corresponding to each
        execution of experiment

    """

    metrics = {}
    for method in parameters.CLASSIFIERS_LIST:
        metrics[method] = {
            "accuracy": [],
            "precision": [],
            "recall": [],
            "f1_score_macro": [],
            "f1_score_micro": []
        }

    for i in range(0, len(best_results)):
        result = best_results[i]['performance_on_test']['single_cluster_result']
        metrics_list = ["accuracy","precision","recall","f1_score_macro", "f1_score_micro"]

        # TODO: Pensar numa melhor forma de guardar os valores
        # 0 = KNN, 1 = NB
        for metric in metrics_list:
            metrics["KNN"][metric].append(result[0][metric])
            metrics["NB"][metric].append(result[1][metric])
            # metrics["RadialSVM"][metric].append(result[2][metric])
            metrics["LinearSVM"][metric].append(result[2][metric])
            metrics["DT"][metric].append(result[3][metric])

    return metrics



def get_steps_results_array(all_pools):
    """
    Transform the dictionary in an array with best results to calculate the
    average, variance and standard deviation

    Returns:
        The metrics dictionary with array of values corresponding to each
        execution of experiment

    """

    # For each cluster the best classifier and metrics
    metrics_template = {
        # performance of classification
        "accuracy": [],
        "precision": [],
        "recall": [],
        "f1_score_macro": [],
        "f1_score_micro": [],
        "classification_method":[],

        # dispersion metrics for cluster
        # "silhouette":[],
        "average_centroid_distance": [],
        "pairwise_centroid_distance": [],
        "pairwise_intracluster_distance": [],
        "pairwise_intracluster_distance_stdev": [],
        "pairwise_intracluster_distance_variance": [],
    }

    results = {}

    for pool in all_pools:
        for iteration in pool:    # an iteration represents a partition with k clusters
            k = str(iteration['k'])
            if not k in results:
                results[k] = metrics_template  # make a copy from template

            if iteration["silhouette"] != None:
                results[k]["silhouette"].append(iteration['silhouette'])

            results[k]["average_centroid_distance"].append(iteration['average_centroid_distance'])
            results[k]["pairwise_centroid_distance"].append(iteration['pairwise_centroid_distance'])
            results[k]["pairwise_intracluster_distance"].append(iteration['pairwise_intracluster_distance'])
            results[k]["pairwise_intracluster_distance_stdev"].append(iteration['pairwise_intracluster_distance_stdev'])
            results[k]["pairwise_intracluster_distance_variance"].append(iteration['pairwise_intracluster_distance_variance'])

            # Get the metrics for the better classifiers for the partition
            best_classifiers = iteration["best_classifiers_on_partition"]

            results[k]["accuracy"].append(best_classifiers["accuracy"])
            results[k]["precision"].append(best_classifiers["precision"])
            results[k]["recall"].append(best_classifiers["recall"])
            results[k]["f1_score_macro"].append(best_classifiers["f1_score_macro"])
            results[k]["f1_score_micro"].append(best_classifiers["f1_score_micro"])

            # TODO: FIX! It's wrong
            results[k]["classification_method"].append(best_classifiers["method"])


    # log("@@@@@ RESULTS @@@@@")
    # log(results)
    # log("@@@@@@@@@@@@@@@@@@@")

    return results



def get_our_method_results_array(best_results):
    """
    Transform the dictionary in an array with best results to calculate the
    average, variance and standard deviation

    Returns:
        The metrics dictionary with array of values corresponding to each
        execution of experiment

    """

    metrics = {
        "METHOD": {
            "accuracy": [],
            "precision": [],
            "recall": [],
            "f1_score_macro": [],
            "f1_score_micro": []
        }
    }

    for i in range(0, len(best_results)):
        result = best_results[i]['performance_on_test']['method_result']
        metrics_list = ["accuracy", "precision","recall","f1_score_macro", "f1_score_micro"]

        # TODO: Guardar os metodos usados no lugar de 'METHOD'
        # Colocar algo do tipo: KNN+NB+NB
        for metric in metrics_list:
            metrics["METHOD"][metric].append(result[metric])

    return metrics

def plot_final_report(dataset_name, best_results):
    """
    Generate a report of results using plotly library.
    """

    # FINAL RESULTS
    result_matrix = [
        ['Method', 'Accuracy', 'Precision', 'Recall', 'F1-Micro', 'F1-Macro'],
    ]

    metrics = get_best_results_array(best_results)
    our_method_metrics = get_our_method_results_array(best_results)

    # Results without our method - single cluster
    for method in metrics:
        metric = metrics[method]
        result_matrix.append([
            method,
            str("%.3f" % np.mean(metric['accuracy'])),
            str("%.3f" % np.mean(metric['precision'])),
            str("%.3f" % np.mean(metric['recall'])),
            str("%.3f" % np.mean(metric['f1_score_macro'])),
            str("%.3f" % np.mean(metric['f1_score_micro']))
        ])

    # Append results from our method - multiple clusters
    result_matrix.append([
        "METHOD",
        str("%.3f" % np.mean(our_method_metrics["METHOD"]['accuracy'])),
        str("%.3f" % np.mean(our_method_metrics["METHOD"]['precision'])),
        str("%.3f" % np.mean(our_method_metrics["METHOD"]['recall'])),
        str("%.3f" % np.mean(our_method_metrics["METHOD"]['f1_score_macro'])),
        str("%.3f" % np.mean(our_method_metrics["METHOD"]['f1_score_micro']))
    ])

    # Initialize a figure with FF.create_table()
    figure = FF.create_table(result_matrix, height_constant=60)

    # Add graph data
    metrics_names = ["Accuracy", "Precision", "Recall", "F1-Macro", "F1-Micro"]

    # results_avg = []
    # for line in range(1,result_matrix):
    #     results_avg.append([
    #         result_matrix[line][0], # method
    #         result_matrix[line][1], # accuracy
    #         result_matrix[line][2], # precision
    #         result_matrix[line][3], # recall
    #         result_matrix[line][4], # f1-macro
    #         result_matrix[line][5]  # f1-micro
    #     ])

    # KNN = [result_matrix[1][1], result_matrix[1][2], result_matrix[1][3], result_matrix[1][4], result_matrix[1][5]]
    # NB = [result_matrix[2][1], result_matrix[2][2], result_matrix[2][3], result_matrix[2][4], result_matrix[2][5]]
    # OUR_METHOD = [result_matrix[3][1], result_matrix[3][2], result_matrix[3][3], result_matrix[3][4], result_matrix[3][5]]

    # Make traces for graph
    traces = []
    # colors = ["#000000","0099ff","#404040","ffa500"] # workaround - same size than result_matrix
    for i in range(1, len(result_matrix)):
        trace = go.Bar(x=metrics_names, y=result_matrix[i][1:], xaxis='x2', yaxis='y2',
                    # marker=dict(color=colors[i]),
                    name=result_matrix[i][0])

        traces.append(trace)


    # trace1 = go.Bar(x=metrics_names, y=KNN, xaxis='x2', yaxis='y2',
    #                 marker=dict(color='#0099ff'),
    #                 name='KNN')
    # trace2 = go.Bar(x=metrics_names, y=NB, xaxis='x2', yaxis='y2',
    #                 marker=dict(color='#404040'),
    #                 name='NB')
    # trace3 = go.Bar(x=metrics_names, y=OUR_METHOD, xaxis='x2', yaxis='y2',
    #                 marker=dict(color='#ffa500'),
    #                 name='OUR_METHOD')

    # Add trace data to figure
    figure['data'].extend(go.Data(traces))

    # Edit layout for subplots
    figure.layout.yaxis.update({'domain': [0, .45]})
    figure.layout.yaxis2.update({'domain': [.6, 1]})
    # The graph's yaxis2 MUST BE anchored to the graph's xaxis2 and vice versa
    figure.layout.yaxis2.update({'anchor': 'x2'})
    figure.layout.xaxis2.update({'anchor': 'y2'})
    figure.layout.yaxis2.update({'title': 'Performance'})

    figure.layout.yaxis2.update({'range':[0,1]})

    # Update the margins to add a title and see graph x-labels.
    figure.layout.margin.update({'t':75, 'l':50})
    figure.layout.update({'title': 'Final Report'})
    # Update the height because adding a graph vertically will interact with
    # the plot height calculated for the table
    figure.layout.update({'height':800})

    # Plot!
    plot(figure, filename=dataset_name + '_final_report.html', auto_open=False)

    return

def test_benchmark_classifiers(train_samples, train_labels, test_samples, test_labels):

    log("\n\n----- Results for simple classifier -----\n", parameters.LOG_SOFT_DEBUG)

    classifiers = []
    performance = {}
    for method in parameters.CLASSIFIERS_LIST:
        clf_start = os.times()
        classifier = mlw.train_classifier(method, train_samples, train_labels)
        if classifier != None:
            test_prediction = mlw.classify(classifier=classifier["clf"], features=test_samples)
            performance[method] = show_performance_report(test_labels, test_prediction, method, parameters.LOG_SOFT_DEBUG)
        else:
            log("Error: Classifier " + method + " returned None on training", parameters.LOG_ERROR)
        clf_end = os.times()
        # cpu_time = user + sys time
        performance[method]["cpu_time"] = (clf_end[0] - clf_start[0]) + (clf_end[1] - clf_start[1])
        log("\n\n### Cpu Time for %s : %.2fs" % (method, performance[method]["cpu_time"]), parameters.LOG_SOFT_DEBUG)


        samples_by_label = mlw.separate_samples_by_label(train_samples, train_labels)

        if parameters.PLOT_2D_DATA:
            # Just Ploting 2 GLOBS DATASET
            log("Plotting Full Dataset with Classifier...")
            for i in range(0,len(samples_by_label)):
                plt.plot(*zip(*samples_by_label[str(i)]["samples"]), 
                            marker=parameters.MARKER_LIST[i % len(parameters.MARKER_LIST)], #markersize=5,
                            markeredgecolor="#000000", markeredgewidth="0.7",
                            color=parameters.COLOR_PALETTE[i], linestyle='')

            # To plot the classifier
            h = 0.002 # step size in the mesh
            x_min, x_max = (0,1)
            y_min, y_max = (0,1)
            xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
            Z = classifier["clf"].predict(np.c_[xx.ravel(), yy.ravel()])
            
            # Put the result into a color plot
            Z = Z.reshape(xx.shape)
            plt.contourf(xx, yy, Z, cmap=plt.cm.YlGnBu , alpha=.6)

            plt.xlim([0,1])
            plt.ylim([0,1])

            plt.title("Full dataset\nClassifier: " + classifier["method"])
            
            if parameters.PLOT_SAVE_FIG == True:
                plt.savefig(parameters.PLOT_CURRENT_DIR + "full_dataset-clf_" + classifier["method"] + ".png")
                plt.clf()
            else:
                plt.show()


    return performance

def run_experiment(params):

    log("\n\n-------------------------------------------------------------------", parameters.LOG_ALWAYS)
    log("--------------------- BEGINING EXPERIMENT -------------------------", parameters.LOG_ALWAYS)
    log("-------------------------------------------------------------------\n\n", parameters.LOG_ALWAYS)

    log("## DATASET: " + params["dataset"] + "\n", parameters.LOG_ALWAYS)

    performance_average_cluster_by_class = {}
    performance_average_benchmark = {}

    # this is for statistic metrics
    performance_average_cluster_by_class[parameters.CLUSTER_BY_CLASS] = {
        "accuracy": [],
        "precision": [],
        "recall": [],
        "f1_score_micro": [],
        "f1_score_macro": [],
        "support": [],
        "balance": [],
        "cpu_time":[],
    }


    # this is for statistic metrics
    for method in parameters.CLASSIFIERS_LIST:
        performance_average_benchmark[method] = {
            "accuracy": [],
            "precision": [],
            "recall": [],
            "f1_score_micro": [],
            "f1_score_macro": [],
            "support": [],
            "balance": [],
            "cpu_time":[],
        }

    average_method_time = 0
    if parameters.USE_KFOLD:

        kfolds = load_dataset(params)
        count = 0
        global centroids_ 
        centroids_ = None

        for fold in kfolds:
            count += 1
            log("\n####### Fold Number " + str(count) + "\n", parameters.LOG_SOFT_DEBUG)

            if parameters.PLOT_2D_DATA == True:
                # The folder "./plot/" is created on main.py
                parameters.PLOT_CURRENT_DIR = "./plot/" + params["key"] + "/" + params["classification_method"] + "/fold" + str(count) + "/"
                os.makedirs(parameters.PLOT_CURRENT_DIR)

            start = os.times()

            # Get partitioned data
            training_set = fold["training_set"]
            test_set = fold["test_set"]

            if parameters.RUN_BENCHMARK:
                performance = test_benchmark_classifiers(training_set["features"], training_set["labels"],
                                                        test_set["features"], test_set["labels"])
                for method in performance:
                    for metric in performance[method]:
                        performance_average_benchmark[method][metric].append(performance[method][metric])

            if parameters.RUN_METHOD:
                # Generate the pool of classifier accoring to the method
                method_start = os.times()
                pool_of_classifiers = {}
                if parameters.SELECTION_METHOD == parameters.SELECTION_PER_SCORE:
                    pool_of_classifiers = generate_pool(training_set["features"], training_set["labels"])
                    select_best_classifier_per_score(pool_of_classifiers, test_set["features"], test_set["labels"])
                elif parameters.SELECTION_METHOD == parameters.CLUSTER_BY_CLASS:
                    method_start = os.times()
                    pool_of_classifiers = generate_pool_for_one_partition(training_set["features"], training_set["labels"])
                    performance = select_best_classifier_per_score(pool_of_classifiers, test_set["features"], test_set["labels"])
                    method_end = os.times()
                    # cpu_time = user + sys time
                    performance["cpu_time"] = (method_end[0] - method_start[0]) + (method_end[1] - method_start[1])
                    for metric in performance:
                        performance_average_cluster_by_class[parameters.CLUSTER_BY_CLASS][metric].append(performance[metric])
                else:
                    log("\nError: Invalid selection method!", parameters.LOG_ERROR)

                method_end = os.times()
                average_method_time += (method_end[0] - method_start[0]) + (method_end[1] - method_start[1])
                log("\n\n ### Cpu Time for Method: %.2fs" % 
                     ((method_end[0] - method_start[0]) + (method_end[1] - method_start[1])), 
                     parameters.LOG_SOFT_DEBUG)

            end = os.times()
            # cpu_time = user + sys time
            cpu_time = (end[0] - start[0]) + (end[1] - start[1])

            log("\n\n ### Cpu Time for Fold: %.2fs" %(cpu_time), parameters.LOG_SOFT_DEBUG )

        average_method_time = average_method_time / count
    else:
        for i in range(0, parameters.EXPERIMENT_REPETITIONS):
            log("\n####### Experiment Number " + str(i+1) + "\n", parameters.LOG_SOFT_DEBUG)
            exp_start = os.times()

            # Get partitioned data
            partitioned_data = load_dataset(params)
            training_set = partitioned_data["training_set"]
            test_set = partitioned_data["test_set"]
            
            if parameters.RUN_BENCHMARK:
                performance = test_benchmark_classifiers(training_set["features"], training_set["labels"],
                                                        test_set["features"], test_set["labels"])
                for method in performance:
                    for metric in performance[method]:
                        performance_average_benchmark[method][metric].append(performance[method][metric])

            if parameters.RUN_METHOD:
                # Generate the pool of classifier accoring to the method
                method_start = os.times()
                pool_of_classifiers = {}
                if parameters.SELECTION_METHOD == parameters.SELECTION_PER_SCORE:
                    pool_of_classifiers = generate_pool(training_set["features"], training_set["labels"])
                    select_best_classifier_per_score(pool_of_classifiers, test_set["features"], test_set["labels"])
                elif parameters.SELECTION_METHOD == parameters.CLUSTER_BY_CLASS:
                    pool_of_classifiers = generate_pool_for_one_partition(training_set["features"], training_set["labels"])
                    performance = select_best_classifier_per_score(pool_of_classifiers, test_set["features"], test_set["labels"])
                    for metric in performance:
                        performance_average_cluster_by_class[parameters.CLUSTER_BY_CLASS][metric].append(performance[metric])
                else:
                    log("\nError: Invalid selection method!", parameters.LOG_ERROR)

                method_end = os.times()
                average_method_time += ((method_end[0] - method_start[0]) + (method_end[1] - method_start[1]))
                log("\n\n ### Cpu Time for Method: %.2fs" % 
                    ((method_end[0] - method_start[0]) + (method_end[1] - method_start[1])), 
                    parameters.LOG_SOFT_DEBUG)

            exp_end = os.times()
            log("\n\n ### Cpu Time for Experiment: %.2fs" % 
                ((exp_end[0] - exp_start[0]) + (exp_end[1] - exp_start[1])), 
                parameters.LOG_SOFT_DEBUG)

        average_method_time = average_method_time / parameters.EXPERIMENT_REPETITIONS
    
    if parameters.RUN_BENCHMARK:
        log("\n\n<<<< BENCHMARK PERFORMANCE, DATASET: " + params["key"] + " >>>>")
        show_metrics_average(performance_average_benchmark, parameters.LOG_ALWAYS)

    if parameters.RUN_METHOD:
        log("\n\n<<<< METHOD PERFORMANCE, DATASET: " + params["key"] + " >>>>")
        log("\n>> Average Time = %.2fs" % (average_method_time), parameters.LOG_SOFT_DEBUG)
        show_metrics_average(performance_average_cluster_by_class, parameters.LOG_ALWAYS)

    log("\n\n-------------------------------------------------------------------", parameters.LOG_ALWAYS)
    log("------------------------ END OF EXPERIMENT ------------------------", parameters.LOG_ALWAYS)
    log("-------------------------------------------------------------------\n", parameters.LOG_ALWAYS)

    return True

def run(params):

    if parameters.PLOT_2D_DATA == True:
        # The folder "./plot/" is created on main.py
        parameters.PLOT_CURRENT_DIR = "./plot/" + params["key"] + "/"
        os.makedirs(parameters.PLOT_CURRENT_DIR)

    if parameters.USE_ENSEMBLE == True:
        run_experiment(params)
    else:
        classifier_list = copy.deepcopy(parameters.CLASSIFIERS_LIST)
        for classifier in classifier_list:
            try:
                # to put separate plots by classifier
                if parameters.PLOT_2D_DATA == True:
                    # The folder "./plot/" is created on main.py
                    parameters.PLOT_CURRENT_DIR = "./plot/" + params["key"] + "/" + classifier + "/"
                    os.makedirs(parameters.PLOT_CURRENT_DIR)
                    
                # workaround to not change the parameter
                parameters.CLASSIFIERS_LIST = [classifier]
                params["classification_method"] = classifier
                run_experiment(params)
            except Exception as e:
                log("Error: Classifier " + classifier + " was skipped!", parameters.LOG_ERROR)
                
                if parameters.LOG_LEVEL == parameters.LOG_FULL_DEBUG:
                    log("Traceback: " + traceback.format_exc())
                    log("Exception message: " + str(e), parameters.LOG_ERROR)

        # back to original list
        parameters.CLASSIFIERS_LIST = classifier_list

    return True