import csv
import numpy as np
import parameters

BALANCE_STDEV_THRESHOLD = parameters.BALANCE_STDEV_THRESHOLD
ORIGINAL_DATASETS_PATH = parameters.ORIGINAL_DATASETS_PATH

DATASETS = parameters.DATASETS

# DUPLICATED
def is_balanced(labels):
    total_of_elements = len(labels)
    percentage_list = []
    result = True

    # print("Support/Balance per class")
    for label in set(labels):
        support = list(labels).count(label)
        percentage = (support/total_of_elements)*100
        percentage_list.append(percentage)
        # print("Class: %d \t %d/%d \t %.2f%% %s"
        #      % (label, support, total_of_elements, percentage, warn))

    stdev_percentage = np.std(percentage_list)
    if stdev_percentage > BALANCE_STDEV_THRESHOLD:
        result = False

    # print("STDEV: %.3f" % np.std(percentage_list))
    # print("VAR: %.3f" % np.var(perc_list))

    return result

def show_datasets():

    # Categories
    balanced = []
    unbalanced = []
    binary = []
    multiclass = []
    few_features = []
    many_features = []
    few_samples = []
    many_samples = []
    # real = []
    # artificial []

    # thresholds
    MANY_FEATURES = 40
    MANY_SAMPLES = 2000

    ALL_DATASETS = False

    print("DATASETS INFO:")
    for ds in sorted(DATASETS):
        if ALL_DATASETS or (ds in parameters.DATASETS_LIST):
            data = csv.reader(open(ORIGINAL_DATASETS_PATH + DATASETS[ds]["dataset"]), delimiter=DATASETS[ds]["delimiter"])
            count = 0
            labels = []

            # Check if dataset is from Lasid
            if "lasid" in DATASETS[ds]["dataset"]:
                # Consider that labels_col will be the name of true partitions file (.clu)
                true_partitions = csv.reader(open(ORIGINAL_DATASETS_PATH + DATASETS[ds]["labels_col"]), delimiter="\t")
                for row in true_partitions:
                    if len(row) > 0:
                        labels.append(row[1])  # for lasid datasets, it's always the second column
            else:
                for row in data:
                    if len(row) > 0:
                        labels.append(row[DATASETS[ds]["labels_col"]])
            
            name = ds
            nsamples = len(labels)
            nfeatures = DATASETS[ds]["features_last_col"] - DATASETS[ds]["features_first_col"] + 1
            nclasses = len(set(labels))
            isbalanced = is_balanced(labels)
            

            print("\ndataset: %s, \n -samples: %d, \n -features: %d, \n -classes: %d, \n -balanced: %s" 
                % (name, nsamples, nfeatures, nclasses, isbalanced))

            # Add datasets on categories list
            if isbalanced:
                balanced.append(name)
            else:
                unbalanced.append(name)

            if nclasses == 2:
                binary.append(name)
            else:
                multiclass.append(name)

            if nfeatures > MANY_FEATURES:
                many_features.append(name)
            else:
                few_features.append(name)

            if nsamples > MANY_SAMPLES:
                many_samples.append(name)
            else:
                few_samples.append(name)

    print("\nDATASETS BY CATEGORY:")
    print("\n balanced: " + str(balanced))
    print("\n unbalanced: " + str(unbalanced))
    print("\n binary: " + str(binary))
    print("\n multiclass: " + str(multiclass))
    print("\n many_features: " + str(many_features))
    print("\n few_features: " + str(few_features))
    print("\n many_samples: " + str(many_samples))
    print("\n few_samples: " + str(few_samples))
            


if __name__ == '__main__':
    show_datasets()