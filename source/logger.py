"""
This file contains the metods for log and debbuging
"""
import parameters

log_message = ""

def log(message, level=parameters.LOG_ALWAYS):
    if level <= parameters.LOG_LEVEL:
        global log_message
        log_message = log_message + "\n" + str(message)
        print(str(message))

