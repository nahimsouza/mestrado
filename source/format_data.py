"""
=============
Format Data
=============

It contains methods to format the data that will be used in the process.

"""

import csv
import json
import os
import numpy as np

from random import shuffle
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import scale
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import StratifiedKFold

import parameters
from logger import log

def load_dataset(dataset, labels_col, features_first_col,
                    features_last_col, delimiter=","):
    """
    Loads the dataset from CSV format to json to create a standard for all
    datasets used

    Returns:
        A json file with the formatted data.
    """

    log("Loading dataset: '" + parameters.ORIGINAL_DATASETS_PATH + dataset + "'", parameters.LOG_SOFT_DEBUG)

    data = csv.reader(open(parameters.ORIGINAL_DATASETS_PATH + dataset), delimiter=delimiter)

    features = []
    labels = []
    number_of_features = features_last_col - features_first_col + 1

    # Check if dataset is from Lasid
    if "lasid" in dataset:
        # Consider that labels_col will be the name of true partitions file (.clu)
        true_partitions = csv.reader(open(parameters.ORIGINAL_DATASETS_PATH + labels_col), delimiter="\t")
        for row in true_partitions:
            if len(row) > 0:
                labels.append(row[1])  # for lasid datasets, it's always the second column

        counter = 0  # count the number of lines to skip header line
        for row in data:
            counter += 1
            if len(row) > 0 and counter > 1:
                features.append(row[features_first_col:features_last_col+1])
    else:
        for row in data:
            if len(row) > 0:
                labels.append(row[labels_col])
                if number_of_features == 1:
                    features.append(row[features_first_col]) # because first and last col are the same
                else:
                    features.append(row[features_first_col:features_last_col+1])

    # If the data is a string, generates a bag of words

    first_sample_features = features[0][0]
    if (number_of_features == 1 and isinstance(first_sample_features, str)):
        log("Generating BOW...", parameters.LOG_SOFT_DEBUG)
        features = generate_bow(features)

    # log("Features: " + str(features), parameters.LOG_FULL_DEBUG)

    # encode labels:
    label_encoder = LabelEncoder().fit(labels)
    encoded_labels = label_encoder.transform(labels)

    formated_data = {
        "dataset": dataset,
        "classes": label_encoder.classes_.tolist(),
        "features": features,
        "labels": encoded_labels.tolist()
    }


    if not os.path.exists(parameters.FORMATED_DATASETS_PATH):
        os.makedirs(parameters.FORMATED_DATASETS_PATH)

    formated_file_name = dataset + ".json"
    if "lasid" in dataset:
        formated_file_name = os.path.basename(dataset) + ".json"

    formated_file_path = parameters.FORMATED_DATASETS_PATH + formated_file_name
    formated_file = open(formated_file_path, 'w+')
    formated_file.write(json.dumps(formated_data))
    formated_file.close()

    log("Destination: '" + formated_file_name + "'", parameters.LOG_SOFT_DEBUG)
    log("Dataset was loaded successfully!", parameters.LOG_SOFT_DEBUG)

    return formated_file_name

def generate_bow(features):
    # Create a table of occurences

    log("Generating BOW...", parameters.LOG_SOFT_DEBUG)
    log("Features: " + str(features), parameters.LOG_FULL_DEBUG)

    count_vect = CountVectorizer()
    X_train_counts = count_vect.fit_transform(features)

    # Create the table of frequencies (TF-IDF = Term Frequency - Inverse Document Frequency)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)

    bow = X_train_tfidf.toarray().tolist()

    # log("BAG OF WORDS: ", parameters.LOG_FULL_DEBUG)
    # log(str(bow), parameters.LOG_FULL_DEBUG)

    return bow

def generate_kfold(dataset, number_of_folds=5, output_file="", scale=False):
    file = open(parameters.FORMATED_DATASETS_PATH + dataset, "r")
    data_json = json.loads(file.read())
    file.close()


    fold_partitioned_data = []
    kfold = StratifiedKFold(n_splits=number_of_folds, shuffle=parameters.KFOLD_SHUFFLE)
    for train_index, test_index in kfold.split(data_json["features"], data_json["labels"]):
        log("TRAIN:" + str(train_index) +  "TEST:" + str(test_index), parameters.LOG_FULL_DEBUG)

        percent_training = str("%.1f" % (len(train_index)/len(data_json["labels"]) * 100))
        percent_test = str("%.1f" % (len(test_index)/len(data_json["labels"]) * 100))
        partitioned_data = {}

        if scale==True:
            partitioned_data = {
                "dataset": dataset,
                "classes": data_json["classes"],
                "training_set": {
                    "percentage": percent_training,
                    "features": MinMaxScaler().fit_transform([data_json["features"][i] for i in train_index]),
                    "labels": [data_json["labels"][i] for i in train_index]
                },
                "test_set": {
                    "percentage": percent_test,
                    "features": MinMaxScaler().fit_transform([data_json["features"][i] for i in test_index]),
                    "labels": [data_json["labels"][i] for i in test_index]
                }
            }
        else:
            partitioned_data = {
                "dataset": dataset,
                "classes": data_json["classes"],
                "training_set": {
                    "percentage": percent_training,
                    "features": np.array([data_json["features"][i] for i in train_index], dtype=float), # Not scale here
                    "labels": [data_json["labels"][i] for i in train_index]
                },
                "test_set": {
                    "percentage": percent_test,
                    "features": np.array([data_json["features"][i] for i in test_index], dtype=float),
                    "labels": [data_json["labels"][i] for i in test_index]
                }
            }

        fold_partitioned_data.append(partitioned_data)

    if output_file != "":
        partitioned_file_name = output_file + ".json"
        partitioned_file = open(output_file, 'w+')
        partitioned_file.write(json.dumps(fold_partitioned_data))
        partitioned_file.close()

    return fold_partitioned_data

def generate_subsets(dataset, training_size=80, selection_size=10, test_size=10, output_file="", data_scale=False):
    file = open(parameters.FORMATED_DATASETS_PATH + dataset, "r")
    data_json = json.loads(file.read())
    file.close()

    # group the samples according to the labels
    number_of_classes = len(data_json["classes"])
    samples_ids = {}
    for index in range(len(data_json["labels"])):
        for label in range(number_of_classes):
            if data_json["labels"][index] == label:
                samples_ids.setdefault(label,[]).append(index)

    training_set_ids = []
    selection_set_ids = []
    test_set_ids = []

    # shuffle indexes to generate random partitions
    for i in samples_ids:
        shuffle(samples_ids[i])

        number_of_training_samples = int(len(samples_ids[i])*training_size/100)
        number_of_selection_samples = int(len(samples_ids[i])*selection_size/100)
        number_of_test_samples = int(len(samples_ids[i])*test_size/100)

        # get the list of IDs for each partition according to percentages
        # training[0:X], selection[X:Y], test[Y:end]
        training_set_ids.extend(samples_ids[i][0:number_of_training_samples])
        selection_set_ids.extend(samples_ids[i][number_of_training_samples:(number_of_training_samples+number_of_selection_samples)])
        test_set_ids.extend(samples_ids[i][(number_of_training_samples+number_of_selection_samples):])

        if (len(selection_set_ids)==0):
            selection_set_ids = training_set_ids

    # TODO::: Acho que da pra guardar os IDs, talvez seja interessante para referenciar os originais

    partitioned_data = {}
    
    if data_scale==True:
        partitioned_data = {
            "dataset": dataset,
            "classes": data_json["classes"],
            "training_set": {
                "percentage": training_size,
                "features": MinMaxScaler().fit_transform([data_json["features"][i] for i in training_set_ids]),
                "labels": [data_json["labels"][i] for i in training_set_ids]
            },
            "selection_set": {
                "percentage": selection_size,
                "features": MinMaxScaler().fit_transform([data_json["features"][i] for i in selection_set_ids]),
                "labels": [data_json["labels"][i] for i in selection_set_ids]
            },
            "test_set": {
                "percentage": test_size,
                "features": MinMaxScaler().fit_transform([data_json["features"][i] for i in test_set_ids]),
                "labels": [data_json["labels"][i] for i in test_set_ids]
            }
        }
    else:
        partitioned_data = {
            "dataset": dataset,
            "classes": data_json["classes"],
            "training_set": {
                "percentage": training_size,
                "features": np.array([data_json["features"][i] for i in training_set_ids], dtype=float), # Not scale here
                "labels": [data_json["labels"][i] for i in training_set_ids]
            },
            "selection_set": {
                "percentage": selection_size,
                "features": np.array([data_json["features"][i] for i in selection_set_ids], dtype=float),
                "labels": [data_json["labels"][i] for i in selection_set_ids]
            },
            "test_set": {
                "percentage": test_size,
                "features": np.array([data_json["features"][i] for i in test_set_ids], dtype=float),
                "labels": [data_json["labels"][i] for i in test_set_ids]
            }
        }

    if output_file != "":
        partitioned_file_name = output_file + ".json"
        partitioned_file = open(output_file, 'w+')
        partitioned_file.write(json.dumps(partitioned_data))
        partitioned_file.close()

    return partitioned_data


#if __name__ == "__main__":
    # check args
    # do something


    #1 Metodo para pegar todos os dados originais e padronizar (csv)
    # prepare_data -load -d "/iris/iris.data" -class 0 -first 1 -last 100
    # o metodo deve verificar se a classe possui atributos textuais(?)
    # -- ver como o scikit trata, mas acho que e necessario
    # a implementacao e semelhante ao load_data()

    #2 Metodo para montar as particoes com amostras aleatorias (80/10/10)
    # prepare_data -partition 80 10 10 -d "/formated/iris.csv"
    # **** por enquanto NAO sera totalmente generico, vai dar muito trabablho para criar o nome das particoes
    #            sempre sera tres particoes: treino, teste, selecao-de-modelo
    # e praticamente copiar o que esta implementado em preprareTrainingAndTestDatasets()
    # precisa deixar generico a criacao/organizacao das pastas
    # a ideia e que esse metodo use os dados padronizados em /formated_data/
    #         a primeira coluna sera a classe, e o restante os atributos
    # 'dataset' deve ser somente o nome: "iris", considerarei a pasta default (infelizmente, por causa do tempo)
