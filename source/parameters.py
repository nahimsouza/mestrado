"""
This file contains all parameters used to run the experiments
"""

######################################################
#                     CONSTANTS
######################################################

DATASETS = {
    "2globs": {
        "dataset":"2globs.txt",
        "labels_col":3,
        "features_first_col":1,
        "features_last_col":2,
        "delimiter":"\t"
    },
    "wine": {
        "dataset": "wine.data",
        "labels_col":0,
        "features_first_col":1,
        "features_last_col":13,
        "delimiter":","
    },
    "iris": {
        "dataset": "iris.data",
        "labels_col":4,
        "features_first_col":0,
        "features_last_col":3,
        "delimiter":","
    },
    "wdbc": {
        "dataset": "wdbc.data",
        "labels_col":1,
        "features_first_col":2,
        "features_last_col":31,
        "delimiter":","
    },
    "breast-cancer-wisconsin": {
        "dataset": "breast-cancer-wisconsin.data",
        "labels_col":10,
        "features_first_col":1,
        "features_last_col":9,
        "delimiter":","
    },

    # LASID DATASETS
    # Artificial
    "atom": {
        "dataset": "lasid-data/artificial/atom/atom.txt",
        "labels_col": "lasid-data/artificial/atom/atomReal.clu",
        "features_first_col": 1,
        "features_last_col": 3,
        "delimiter":"\t"
    },
    "ds2c2sc13-1": {
        "dataset": "lasid-data/artificial/ds2c2sc13/ds2c2sc13.txt",
        "labels_col": "lasid-data/artificial/ds2c2sc13/ds2c2sc13Real-13classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "ds2c2sc13-2": {
        "dataset": "lasid-data/artificial/ds2c2sc13/ds2c2sc13.txt",
        "labels_col": "lasid-data/artificial/ds2c2sc13/ds2c2sc13Real-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "ds2c2sc13-3": {
        "dataset": "lasid-data/artificial/ds2c2sc13/ds2c2sc13.txt",
        "labels_col": "lasid-data/artificial/ds2c2sc13/ds2c2sc13Real-5classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "ds3c3sc6-1": {
        "dataset": "lasid-data/artificial/ds3c3sc6/ds3c3sc6.txt",
        "labels_col": "lasid-data/artificial/ds3c3sc6/ds3c3sc6Real-3classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "ds3c3sc6-2": {
        "dataset": "lasid-data/artificial/ds3c3sc6/ds3c3sc6.txt",
        "labels_col": "lasid-data/artificial/ds3c3sc6/ds3c3sc6Real-6classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "ds4c2sc8-1": {
        "dataset": "lasid-data/artificial/ds4c2sc8/ds4c2sc8.txt",
        "labels_col": "lasid-data/artificial/ds4c2sc8/ds4c2sc8Real-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "ds4c2sc8-2": {
        "dataset": "lasid-data/artificial/ds4c2sc8/ds4c2sc8.txt",
        "labels_col": "lasid-data/artificial/ds4c2sc8/ds4c2sc8Real-8classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "engyTime": {
        "dataset": "lasid-data/artificial/engyTime/engyTime.txt",
        "labels_col": "lasid-data/artificial/engyTime/engyTimeReal.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "gaussian3": {
        "dataset": "lasid-data/artificial/gaussian3/gaussian3.txt",
        "labels_col": "lasid-data/artificial/gaussian3/gaussian3Real.clu",
        "features_first_col": 1,
        "features_last_col": 600,
        "delimiter":"\t"
    },
    "hepta": {
        "dataset": "lasid-data/artificial/hepta/hepta.txt",
        "labels_col": "lasid-data/artificial/hepta/heptaReal.clu",
        "features_first_col": 1,
        "features_last_col": 3,
        "delimiter":"\t"
    },
    "lsun": {
        "dataset": "lasid-data/artificial/lsun/lsun.txt",
        "labels_col": "lasid-data/artificial/lsun/lsunReal.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "monkey-1": {
        "dataset": "lasid-data/artificial/monkey/monkey.txt",
        "labels_col": "lasid-data/artificial/monkey/monkeyReal1.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "monkey-2": {
        "dataset": "lasid-data/artificial/monkey/monkey.txt",
        "labels_col": "lasid-data/artificial/monkey/monkeyReal2.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "monkey-3": {
        "dataset": "lasid-data/artificial/monkey/monkey.txt",
        "labels_col": "lasid-data/artificial/monkey/monkeyReal3.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "monkey-4": {
        "dataset": "lasid-data/artificial/monkey/monkey.txt",
        "labels_col": "lasid-data/artificial/monkey/monkeyReal4.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "simulated6": {
        "dataset": "lasid-data/artificial/simulated6/simulated6.txt",
        "labels_col": "lasid-data/artificial/simulated6/simulated6Real.clu",
        "features_first_col": 1,
        "features_last_col": 600,
        "delimiter":"\t"
    },
    "spiralsquare-1": {
        "dataset": "lasid-data/artificial/spiralsquare/spiralsquare.txt",
        "labels_col": "lasid-data/artificial/spiralsquare/spiralsquareReal-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "spiralsquare-2": {
        "dataset": "lasid-data/artificial/spiralsquare/spiralsquare.txt",
        "labels_col": "lasid-data/artificial/spiralsquare/spiralsquareReal-6classes.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "target": {
        "dataset": "lasid-data/artificial/target/target.txt",
        "labels_col": "lasid-data/artificial/target/targetReal.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "tetra": {
        "dataset": "lasid-data/artificial/tetra/tetra.txt",
        "labels_col": "lasid-data/artificial/tetra/tetraReal.clu",
        "features_first_col": 1,
        "features_last_col": 3,
        "delimiter":"\t"
    },
    "twoDiamonds": {
        "dataset": "lasid-data/artificial/twoDiamonds/twoDiamonds.txt",
        "labels_col": "lasid-data/artificial/twoDiamonds/twoDiamondsReal.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },
    "wingNut": {
        "dataset": "lasid-data/artificial/wingNut/wingNut.txt",
        "labels_col": "lasid-data/artificial/wingNut/wingNutReal.clu",
        "features_first_col": 1,
        "features_last_col": 2,
        "delimiter":"\t"
    },

    # Real
    "armstrong-1": {
        "dataset": "lasid-data/real/armstrong/armstrong-2002.txt",
        "labels_col": "lasid-data/real/armstrong/armstrong-2002Real1.clu",
        "features_first_col": 1,
        "features_last_col": 1081,
        "delimiter":"\t"
    },
    "armstrong-2": {
        "dataset": "lasid-data/real/armstrong/armstrong-2002.txt",
        "labels_col": "lasid-data/real/armstrong/armstrong-2002Real2.clu",
        "features_first_col": 1,
        "features_last_col": 1081,
        "delimiter":"\t"
    },
    "chowdary": {
        "dataset": "lasid-data/real/chowdary/chowdary-2006.txt",
        "labels_col": "lasid-data/real/chowdary/chowdary-2006-classes.clu",
        "features_first_col": 1,
        "features_last_col": 182,
        "delimiter":"\t"
    },
    "contractions": {
        "dataset": "lasid-data/real/contractions/contractions.txt",
        "labels_col": "lasid-data/real/contractions/contractionsReal.clu",
        "features_first_col": 1,
        "features_last_col": 27,
        "delimiter":"\t"
    },
    "dyrskjot": {
        "dataset": "lasid-data/real/dyrskjot/dyrskjot-2003.txt",
        "labels_col": "lasid-data/real/dyrskjot/dyrskjot-2003-classes.clu",
        "features_first_col": 1,
        "features_last_col": 1203,
        "delimiter":"\t"
    },
    "eTongueSugar-1": {
        "dataset": "lasid-data/real/eTongueSugar/eTongueSugar.txt",
        "labels_col": "lasid-data/real/eTongueSugar/eTongueSugarReal-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 6,
        "delimiter":"\t"
    },
    "eTongueSugar-2": {
        "dataset": "lasid-data/real/eTongueSugar/eTongueSugar.txt",
        "labels_col": "lasid-data/real/eTongueSugar/eTongueSugarReal-3classes.clu",
        "features_first_col": 1,
        "features_last_col": 6,
        "delimiter":"\t"
    },
    "glass-1": {
        "dataset": "lasid-data/real/glass/glass.txt",
        "labels_col": "lasid-data/real/glass/glassReal-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 9,
        "delimiter":"\t"
    },
    "glass-2": {
        "dataset": "lasid-data/real/glass/glass.txt",
        "labels_col": "lasid-data/real/glass/glassReal-5classes.clu",
        "features_first_col": 1,
        "features_last_col": 9,
        "delimiter":"\t"
    },
    "glass-3": {
        "dataset": "lasid-data/real/glass/glass.txt",
        "labels_col": "lasid-data/real/glass/glassReal-6classes.clu",
        "features_first_col": 1,
        "features_last_col": 9,
        "delimiter":"\t"
    },
    "golub-1": {
        "dataset": "lasid-data/real/golub/golub.txt",
        "labels_col": "lasid-data/real/golub/golubReal-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 3571,
        "delimiter":"\t"
    },
    "golub-2": {
        "dataset": "lasid-data/real/golub/golub.txt",
        "labels_col": "lasid-data/real/golub/golubReal-2classes2.clu",
        "features_first_col": 1,
        "features_last_col": 3571,
        "delimiter":"\t"
    },
    "golub-3": {
        "dataset": "lasid-data/real/golub/golub.txt",
        "labels_col": "lasid-data/real/golub/golubReal-3classes.clu",
        "features_first_col": 1,
        "features_last_col": 3571,
        "delimiter":"\t"
    },
    "golub-4": {
        "dataset": "lasid-data/real/golub/golub.txt",
        "labels_col": "lasid-data/real/golub/golubReal-4classes.clu",
        "features_first_col": 1,
        "features_last_col": 3571,
        "delimiter":"\t"
    },
    "gordon": {
        "dataset": "lasid-data/real/gordon/gordon-2002.txt",
        "labels_col": "lasid-data/real/gordon/gordon-2002-classes.clu",
        "features_first_col": 1,
        "features_last_col": 1626,
        "delimiter":"\t"
    },
    "iris-2": {
        "dataset": "lasid-data/real/iris/iris.txt",
        "labels_col": "lasid-data/real/iris/irisReal.clu",
        "features_first_col": 1,
        "features_last_col": 4,
        "delimiter":"\t"
    },
    "laryngeal1": {
        "dataset": "lasid-data/real/laryngeal1/laryngeal1.txt",
        "labels_col": "lasid-data/real/laryngeal1/laryngeal1Real.clu",
        "features_first_col": 1,
        "features_last_col": 16,
        "delimiter":"\t"
    },
    "laryngeal2": {
        "dataset": "lasid-data/real/laryngeal2/laryngeal2.txt",
        "labels_col": "lasid-data/real/laryngeal2/laryngeal2Real.clu",
        "features_first_col": 1,
        "features_last_col": 16,
        "delimiter":"\t"
    },
    "laryngeal3-1": {
        "dataset": "lasid-data/real/laryngeal3/laryngeal3.txt",
        "labels_col": "lasid-data/real/laryngeal3/laryngeal3Real-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 16,
        "delimiter":"\t"
    },
    "laryngeal3-2": {
        "dataset": "lasid-data/real/laryngeal3/laryngeal3.txt",
        "labels_col": "lasid-data/real/laryngeal3/laryngeal3Real-3classes.clu",
        "features_first_col": 1,
        "features_last_col": 16,
        "delimiter":"\t"
    },
    "libras-1": {
        "dataset": "lasid-data/real/libras/libras.txt",
        "labels_col": "lasid-data/real/libras/movement_librasReal-8classes.clu",
        "features_first_col": 1,
        "features_last_col": 90,
        "delimiter":"\t"
    },
    "libras-2": {
        "dataset": "lasid-data/real/libras/libras.txt",
        "labels_col": "lasid-data/real/libras/movement_librasReal-15classes.clu",
        "features_first_col": 1,
        "features_last_col": 90,
        "delimiter":"\t"
    },
    "lung": {
        "dataset": "lasid-data/real/lung/lung.txt",
        "labels_col": "lasid-data/real/lung/lungReal-4classes.clu",
        "features_first_col": 1,
        "features_last_col": 1000,
        "delimiter":"\t"
    },
    "miRNAcancer-1": {
        "dataset": "lasid-data/real/miRNAcancer/miRNAcancer.txt",
        "labels_col": "lasid-data/real/miRNAcancer/miRNAcancerRealCLT.clu",
        "features_first_col": 1,
        "features_last_col": 217,
        "delimiter":"\t"
    },
    "miRNAcancer-2": {
        "dataset": "lasid-data/real/miRNAcancer/miRNAcancer.txt",
        "labels_col": "lasid-data/real/miRNAcancer/miRNAcancerRealEP.clu",
        "features_first_col": 1,
        "features_last_col": 217,
        "delimiter":"\t"
    },
    "miRNAcancer-3": {
        "dataset": "lasid-data/real/miRNAcancer/miRNAcancer.txt",
        "labels_col": "lasid-data/real/miRNAcancer/miRNAcancerRealGI.clu",
        "features_first_col": 1,
        "features_last_col": 217,
        "delimiter":"\t"
    },
    "miRNAcancer-4": {
        "dataset": "lasid-data/real/miRNAcancer/miRNAcancer.txt",
        "labels_col": "lasid-data/real/miRNAcancer/miRNAcancerRealMAL.clu",
        "features_first_col": 1,
        "features_last_col": 217,
        "delimiter":"\t"
    },
    "miRNAcancer-5": {
        "dataset": "lasid-data/real/miRNAcancer/miRNAcancer.txt",
        "labels_col": "lasid-data/real/miRNAcancer/miRNAcancerRealSCC.clu",
        "features_first_col": 1,
        "features_last_col": 217,
        "delimiter":"\t"
    },

    # ### ERROR! THIS DATASET DOES NOT WORK!
    "miRNAcancer-6": {
        "dataset": "lasid-data/real/miRNAcancer/miRNAcancer.txt",
        "labels_col": "lasid-data/real/miRNAcancer/miRNAcancerRealTT.clu",
        "features_first_col": 1,
        "features_last_col": 217,
        "delimiter":"\t"
    },

    "respiratory": {
        "dataset": "lasid-data/real/respiratory/respiratory.txt",
        "labels_col": "lasid-data/real/respiratory/respiratoryReal.clu",
        "features_first_col": 1,
        "features_last_col": 17,
        "delimiter":"\t"
    },
    "segmentation": {
        "dataset": "lasid-data/real/segmentation/segmentation.txt",
        "labels_col": "lasid-data/real/segmentation/segmentationReal.clu",
        "features_first_col": 1,
        "features_last_col": 19,
        "delimiter":"\t"
    },
    "su": {
        "dataset": "lasid-data/real/su/su-2001.txt",
        "labels_col": "lasid-data/real/su/su-2001-classes.clu",
        "features_first_col": 1,
        "features_last_col": 1571,
        "delimiter":"\t"
    },
    "voice3-1": {
        "dataset": "lasid-data/real/voice3/voice3.txt",
        "labels_col": "lasid-data/real/voice3/voice3Real-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 10,
        "delimiter":"\t"
    },
    "voice3-2": {
        "dataset": "lasid-data/real/voice3/voice3.txt",
        "labels_col": "lasid-data/real/voice3/voice3Real-3classes.clu",
        "features_first_col": 1,
        "features_last_col": 10,
        "delimiter":"\t"
    },
    "voice9-1": {
        "dataset": "lasid-data/real/voice9/voice9.txt",
        "labels_col": "lasid-data/real/voice9/voice9Real-2classes.clu",
        "features_first_col": 1,
        "features_last_col": 10,
        "delimiter":"\t"
    },
    "voice9-2": {
        "dataset": "lasid-data/real/voice9/voice9.txt",
        "labels_col": "lasid-data/real/voice9/voice9Real-9classes.clu",
        "features_first_col": 1,
        "features_last_col": 10,
        "delimiter":"\t"
    },
    "weaning": {
        "dataset": "lasid-data/real/weaning/weaning.txt",
        "labels_col": "lasid-data/real/weaning/weaningReal.clu",
        "features_first_col": 1,
        "features_last_col": 17,
        "delimiter":"\t"
    },
    "yeoh-1": {
        "dataset": "lasid-data/real/yeoh/yeoh-2002-v1.txt",
        "labels_col": "lasid-data/real/yeoh/yeoh-2002-v1-classes.clu",
        "features_first_col": 1,
        "features_last_col": 2526,
        "delimiter":"\t"
    },
    "yeoh-2": {
        "dataset": "lasid-data/real/yeoh/yeoh-2002-v1.txt",
        "labels_col": "lasid-data/real/yeoh/yeoh-2002-v2-classes.clu",
        "features_first_col": 1,
        "features_last_col": 2526,
        "delimiter":"\t"
    },

    # Datasets 1K+
    "messidor": {
        "dataset": "messidor.csv",
        "labels_col": 19,
        "features_first_col": 0,
        "features_last_col": 18,
        "delimiter":","
    },
    "letter-recognition": {
        "dataset": "letter-recognition.data",
        "labels_col": 0,
        "features_first_col": 1,
        "features_last_col": 16,
        "delimiter":","
    },
    "abalone": {
        "dataset": "abalone_encoded.data",
        "labels_col": 8,
        "features_first_col": 0,
        "features_last_col": 7,
        "delimiter":","
    },
    "credit-card-defaults": {
        "dataset": "credit-card-defaults.csv",
        "labels_col": 24,
        "features_first_col": 1,
        "features_last_col": 23,
        "delimiter": ","
    },

    "credit-card-defaults-10-percent": {
        "dataset": "credit-card-defaults-10-percent.csv",
        "labels_col": 24,
        "features_first_col": 1,
        "features_last_col": 23,
        "delimiter": ","
    },

    "sts-small": {
        "dataset": "sts-small.csv",
        "labels_col": 0,
        "features_first_col": 5,
        "features_last_col": 5,
        "delimiter": ","
    },

    "glass": {
        "dataset": "glass.csv",
        "labels_col": 9,
        "features_first_col": 0,
        "features_last_col": 8,
        "delimiter": ","
    },

    "yeast": {
        "dataset": "yeast.data",
        "labels_col": 9,
        "features_first_col": 1,
        "features_last_col": 8,
        "delimiter": ","
    },

    "winequality-red": {
        "dataset": "winequality-red.csv",
        "labels_col": 11,
        "features_first_col": 0,
        "features_last_col": 10,
        "delimiter": ";"
    },

    "winequality-white": {
        "dataset": "winequality-white.csv",
        "labels_col": 11,
        "features_first_col": 0,
        "features_last_col": 10,
        "delimiter": ";"
    },

    "sensorless-drive-diagnosis": {
        "dataset": "sensorless-drive-diagnosis.txt",
        "labels_col": 48,
        "features_first_col": 0,
        "features_last_col": 47,
        "delimiter": " "  #  delimiter is a blank space
    },

    "sts-16k": {
        "dataset": "sts-6k.csv",
        "labels_col": 0,
        "features_first_col": 1,
        "features_last_col": 1,
        "delimiter": ","
    },

    "mushrooms": {
        "dataset": "mushrooms-encoded.csv",
        "labels_col": 0,
        "features_first_col": 1,
        "features_last_col": 117,
        "delimiter": ","
    },

    "ctg": {
        "dataset": "ctg.csv",
        "labels_col": 39,
        "features_first_col": 3,
        "features_last_col": 38,
        "delimiter": ","
    },

    "parkinsons":{
        "dataset": "parkinson-full.csv",
        "labels_col": 21,
        "features_first_col": 0,
        "features_last_col": 20,
        "delimiter": ","
    },

    "activity-recognition":{
        "dataset": "activity-recognition-full.csv",
        "labels_col": 0,
        "features_first_col": 560,
        "features_last_col": 561,
        "delimiter": ","
    },

    "madelon":{
        "dataset": "madelon-full.csv",
        "labels_col": 500,
        "features_first_col": 0,
        "features_last_col": 499,
        "delimiter": ","
    },

    "pendigits":{
        "dataset": "pendigits.tra",
        "labels_col": 16,
        "features_first_col": 0,
        "features_last_col": 15,
        "delimiter": ","
    },

    "satimage":{
        "dataset": "sat.trn.txt",
        "labels_col": 36,
        "features_first_col": 0,
        "features_last_col": 35,
        "delimiter": " "
    },

    "vowel":{
        "dataset": "vowel.tr-orig-order",
        "labels_col": 10,
        "features_first_col": 0,
        "features_last_col": 9,
        "delimiter": ","
    },

    "waveform":{
        "dataset": "waveform.data",
        "labels_col": 21,
        "features_first_col": 0,
        "features_last_col": 20,
        "delimiter": ","
    },

    "ankleshoe":{
        "dataset": "ankleshoe.dat",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": " "
    },

    "concussion":{
        "dataset": "concussion.dat",
        "labels_col": 4,
        "features_first_col": 0,
        "features_last_col": 3,
        "delimiter": " "
    },

    "micerad":{
        "dataset": "micerad.dat",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": " "
    },

    "nazi":{
        "dataset": "nazi.dat",
        "labels_col": 5,
        "features_first_col": 0,
        "features_last_col": 4,
        "delimiter": " "
    },

    "slash_survsex":{
        "dataset": "slash_survsex.dat",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": " "
    },
    

    # ------ my artificial datasets --------

    "2blobs":{
        "dataset": "2blobs.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "2blobs-inv":{
        "dataset": "2blobs-inv.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "2blobs-inv-2":{
        "dataset": "2blobs-inv-2.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "2blobs4classes":{
        "dataset": "2blobs4classes.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "3blobs":{
        "dataset": "3blobs.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "4blobs3classes":{
        "dataset": "4blobs3classes.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "4blobs2classes":{
        "dataset": "4blobs2classes.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

   "4bl2cl-small":{
        "dataset": "4bl2cl-small.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "4clusters":{
        "dataset": "4clusters.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    "gaussian-4classes":{
        "dataset": "gaussian-4classes.csv",
        "labels_col": 2,
        "features_first_col": 0,
        "features_last_col": 1,
        "delimiter": ","
    },

    # ----
}

# Classifiers string constants
CLASSIFIER_KNN = "KNN"
CLASSIFIER_GAUSSIAN_NB = "GaussianNB"
CLASSIFIER_MULTINOMIAL_NB = "MultinomialNB"
CLASSIFIER_BERNOULLI_NB = "BernoulliNB"
CLASSIFIER_LINEAR_SVM = "LinearSVM"
CLASSIFIER_RBF_SVM = "RadialSVM"
CLASSIFIER_POLYNOMIAL_SVM = "PolynomialSVM"
CLASSIFIER_DT = "DecisionTree"
CLASSIFIER_DUMMY = "DummyClassifier"
CLASSIFIER_LINEAR_REGRESSION = "LinearRegession"
CLASSIFIER_LOGISTIC_REGRESSION = "LogisticRegression"

# Classifier selecion methods
SELECTION_PER_SCORE = "Selction-per-Score"  # uses the classifier score over training set to select the best classifier and the best partition
CLUSTER_BY_CLASS = "Cluster-by-Class"       # generates only partition and selects the best classifier by score on training set

# Log levels
LOG_NONE = 0
LOG_ALWAYS = 1
LOG_ERROR = 2
LOG_WARNING = 3
LOG_SOFT_DEBUG = 4
LOG_FULL_DEBUG = 5

# Paths for datasets
ORIGINAL_DATASETS_PATH = "../resources/original_data/"
FORMATED_DATASETS_PATH = "../resources/formated_data/"

# Colors to plot wrapper
COLOR_MIDDLE_RED_PURPLE = "#380036"
COLOR_RUSTY_RED = "#d7263d"
COLOR_DEEP_SAFRON_ORANGE = "#ffa630"
COLOR_RICH_ELETRIC_BLUE = "#00a5cf"
COLOR_JUNGLE_GREEN = "#25a18e"
COLOR_RAISIN_BLACK = "#2c1320"
COLOR_PASTEL_GREEN = "#7ae582"
COLOR_KIWI = "#a1e44d"
COLOR_MAXIMUM_RED_PURPLE = "#a23b72"
COLOR_TANGERINE = "#f18f01"
COLOR_BOSTON_RED = "#d00000"
COLOR_EGYPTIAN_BLUE = "#0e34a0"
COLOR_AVOCADO = "#5c8001"
COLOR_DANDELION = "#f3de2c"
COLOR_OTHER_CYAN = "#01baef"

# material colors
# https://www.materialpalette.com/colors
COLOR_RED = "#f44336"
COLOR_PINK = "#e91e63"
COLOR_PURPLE = "#9c27b0"
COLOR_DEEP_PURPLE = "#673ab7"
COLOR_INDIGO = "#3f51b5"
COLOR_BLUE = "#2196f3"
COLOR_LIGHT_BLUE = "#03a9f4"
COLOR_CYAN = "#00bcd4"
COLOR_TEAL = "#009688"
COLOR_GREEN = "#4caf50"
COLOR_LIGHT_GREEN = "#8bc34a"
COLOR_LIME = "#cddc39"
COLOR_YELLOW = "#ffeb3b"
COLOR_AMBER = "#ffc107"
COLOR_ORANGE = "#ff9800"
COLOR_DEEP_ORANGE = "#ff5722"
COLOR_BROWN = "#795548"
COLOR_GREY = "#9e9e9e"
COLOR_BLUE_GREY = "#607d8b"

# Custom palette
COLOR_PALETTE = [
    COLOR_LIGHT_GREEN,
    COLOR_ORANGE,
    COLOR_LIGHT_BLUE,
    COLOR_AVOCADO,
    COLOR_EGYPTIAN_BLUE,
    COLOR_KIWI,
    COLOR_RICH_ELETRIC_BLUE,
    COLOR_BOSTON_RED,
    COLOR_JUNGLE_GREEN,
    COLOR_DEEP_SAFRON_ORANGE,
    COLOR_RUSTY_RED,
    COLOR_RAISIN_BLACK,
    COLOR_PASTEL_GREEN,
    COLOR_MIDDLE_RED_PURPLE,
    COLOR_MAXIMUM_RED_PURPLE,
    COLOR_TANGERINE,
    COLOR_DANDELION,
    COLOR_OTHER_CYAN,

    # COLOR_MATERIAL_PALLETE
    COLOR_RED,
    COLOR_INDIGO,
    COLOR_TEAL,
    COLOR_YELLOW,
    COLOR_BROWN,

    COLOR_PINK,
    COLOR_BLUE,
    COLOR_GREEN,
    COLOR_AMBER,
    COLOR_GREY,

    COLOR_PURPLE,

    COLOR_ORANGE,
    COLOR_BLUE_GREY,

    COLOR_DEEP_PURPLE,
    COLOR_CYAN,
    COLOR_LIME,
    COLOR_DEEP_ORANGE,
]

COLOR_MATERIAL_PALETTE = [
    COLOR_RED,
    COLOR_INDIGO,
    COLOR_TEAL,
    COLOR_YELLOW,
    COLOR_BROWN,

    COLOR_PINK,
    COLOR_BLUE,
    COLOR_GREEN,
    COLOR_AMBER,
    COLOR_GREY,

    COLOR_PURPLE,
    COLOR_LIGHT_BLUE,
    COLOR_LIGHT_GREEN,
    COLOR_ORANGE,
    COLOR_BLUE_GREY,

    COLOR_DEEP_PURPLE,
    COLOR_CYAN,
    COLOR_LIME,
    COLOR_DEEP_ORANGE,
]

# Markers to plot wrapper
MARKER_CIRCLE = "o"
MARKER_X = "x"
MARKER_TRIANGLE = "^"
MARKER_SQUARE = "s"
MARKER_TRIANGLE_DOWN = "v"
MARKER_PENTAGON = "p"
MARKER_STAR = "*"
MARKER_HEXAGON_1 = "H"
MARKER_HEXAGON_2 = "h"
MARKER_DIAMOND = "D"
MARKER_TRIANGLE_LEFT = "<"
MARKER_TRIANGLE_RIGHT = ">"
# MARKER_PLUS = "P"

MARKER_LIST = [
    MARKER_CIRCLE,
    MARKER_SQUARE,
    MARKER_TRIANGLE,
    MARKER_TRIANGLE_DOWN,
    MARKER_PENTAGON,
    MARKER_STAR,
    MARKER_HEXAGON_1,
    MARKER_HEXAGON_2,
    MARKER_DIAMOND,
    MARKER_TRIANGLE_LEFT,
    MARKER_TRIANGLE_RIGHT,
    MARKER_X,
    # MARKER_PLUS,
]


######################################################
#                     PARAMETERS
######################################################

# The list of datasets to be used on experiments
DATASETS_LIST = [
    # Real 
    "iris",
    "wdbc",
    "messidor",
    "sts-small",
    "eTongueSugar-2",
    "winequality-white",
    "voice9-2",
    "libras-1",
    "libras-2",
    "weaning",
    "laryngeal1",   # error on multinomial NB
    "laryngeal3-2", # error on multinomial NB
    "yeast",
    "yeoh-2",
    "segmentation",
    "parkinsons",
    "pendigits",
    "satimage",
    "vowel",
    "ankleshoe",
    "micerad",
    "nazi",
    "slash_survsex",
    "madelon",
    "waveform",

    # Artificial Datasets    
    "2globs",
    "monkey-4",
    "ds2c2sc13-1",
    "2blobs",
    "2blobs4classes",
    "gaussian-4classes",
    "4blobs2classes",
    "engyTime",
    "3blobs",
    "4clusters",
    "4blobs3classes",
    # # "2blobs-inv",
    # # "2blobs-inv-2",
    # # "4bl2cl-small",
    

    # Datasets with problems, skipped on classification
    # #### "voice9-1",
    # #### "activity-recognition",
    # #### "monkey-1",
    # #### "spiralsquare-2",
    # #### "target",
    # #### "concussion",

    # # biggest datasets
    # "credit-card-defaults-10-percent", # error on multinomial NB
    # "letter-recognition",
    # "sensorless-drive-diagnosis",     # error on multinomial NB
]


# True for run our method implementation
RUN_METHOD = True

# True for use ensemble, otherwise use individual classifier + method
USE_ENSEMBLE = False

# True for run comparison benchmark with simple classifiers
RUN_BENCHMARK = True

# Set True to plot 2D data
PLOT_2D_DATA = False

# The directory where plots will be saved
PLOT_CURRENT_DIR = "./plot/"

# Set True to save plots,
# Otherwise will show the plots
PLOT_SAVE_FIG = True


# The list of classification methods to be used on experiments
CLASSIFIERS_LIST = [
    CLASSIFIER_KNN,
    CLASSIFIER_DT,
    CLASSIFIER_GAUSSIAN_NB,
    CLASSIFIER_BERNOULLI_NB,
    CLASSIFIER_LOGISTIC_REGRESSION,
    CLASSIFIER_LINEAR_SVM,
    CLASSIFIER_RBF_SVM,

    # CLASSIFIER_POLYNOMIAL_SVM,

    # CLASSIFIER_MULTINOMIAL_NB,    # error: X cannot be negative
    # CLASSIFIER_LINEAR_REGRESSION, # wrong? only for regression?
]

# Set log level, it will log the messages with log level
# less than or equal to the LOG_LEVEL
LOG_LEVEL = LOG_ERROR

# Minimum number of elements per cluster
MIN_ELEMENTS_PER_CLUSTER = 20
MIN_ELEMENTS_PER_CLUSTER_PERCENT = 0.05

# Standard deviation of percentage of samples in each class
# Used to determine if the set of samples is balanced
BALANCE_STDEV_THRESHOLD = 19.0

# Grid search parameters for classifiers
GRID_SVC_MAX_ITER = 200000
GRID_BIN_RANGE = list((10.0**i) for i in range(-3,3))
GRID_RANGE3 = list((10.0**i) for i in range(-3, 3))
GRID_RANGE7 = list((10.0**i) for i in range(-7,7))
GRID_DEGREE_RANGE = list(range(2,5))
GRID_MAX_KNN_RANGE = 21
GRID_KNN_RANGE = list(range(1, GRID_MAX_KNN_RANGE))
GRID_SCORING = "f1_macro"
GRID_KFOLD = 5
GRID_VERBOSE = 0

# This weight corresponds to the 'gravitational force' of the cluster
# The weight is calculated using the Newton's gravitational formula,
# i.e, weight = F = (m1*m2)/d^2
# Where 'm1' and 'm2' are the number of samples in each
# cluster and 'd' is the distance between their centroids
# The higher this value the greater the force required to merge two clusters
# MIN_WEIGHT = 0.02
WEIGHT = 0.01

# Set True if wants to use Weight to remove small clusters
# If False, it will use the minimum distance criteria
USE_WEIGHT = False

# If True uses the percentage of elements as intersection crieteria
# Otherwise, uses distance from centroid as criteria
USE_INTERSECTION_CRITERIA = True

# Intersection criteria, True for use proportion,
# otherwise, uses percentage
INTERSECTION_BY_PROPORTION = True

# Makes intersection recalculating centroid for each iteration
USE_CENTROID_RECALCULATION = False

# Percentage of intersection between clusters
# Used to determine if clusters can be merged
INTERSECTION_CRITERIA_PERCENTAGE = 0.2

# Proportion between clusters, considering size and distance
INTERSECTION_CRITERIA_PROPORTION = 0.5

# Determine if smote must be used
USE_SMOTE = True

# Determine if the dataset entries will be scaled (normalized)
NORMALIZE_DATA = True

# Determine if TF-IDF will be used to generate BOW on text data
USE_TFIDF = True

# Number of iterations on kmeans
KMEANS_MAX_ITERATIONS = 50

# Size of subsets in percentage
TRAINING_SET_SIZE = 80
SELECTION_SET_SIZE = 0  # deprecated
TEST_SET_SIZE = 20

# Set selection method to be used on experiments
SELECTION_METHOD = CLUSTER_BY_CLASS

# Set max and min number of clusters for generate partitions
# on selecion_per_score method
MIN_NUMBER_OF_CLUSTERS = 1
MAX_NUMBER_OF_CLUSTERS = 20  # used for cluster-by-class too
USE_MAX_CLUSTER_DEFAULT = False

# Set True to use KFold to measure performance on benchmark
# instead of random sampling
USE_KFOLD = True
NUMBER_OF_FOLDS = 5
KFOLD_SHUFFLE = True

# Number of times experiment will be repeated
# for statistic validation (only if not using k-fold)
EXPERIMENT_REPETITIONS = 1

# Set True to use GAP statistics to find better K value
# Otherwise, uses elbow method
USE_GAP_STATISTICS = False

# Elbow method percentage
ELBOW_PERCENTAGE = 10

