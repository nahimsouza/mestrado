___

# P2C - Partitioning to Classify

This work describes a new classification technique called P2C - Partitioning to Classify. 
The main goal is to achieve reasonable classification performances using linear prediction methods, 
even on datasets with non-linear separable data. The proposed technique, inspired by the 
division-and-conquer strategy, applies a clustering method on each partition made of samples 
of the same class. Subsequently, the union among the clusters inside each partition is performed,
creating a single partition, where each group can contain linearly separable samples. 
Then, one or more linear classifiers are trained, according to the number of groups. 
Experiments performed using datasets with different structural and complexity level indicate 
the overall performance of the prediction is similar or superior to well-known non-linear 
classification methods. The main advantages of P2C technique are (i) the need for less effort 
and computational resources, and (ii) the possibility of treating large datasets due to the ease 
of parallelization of the steps.

This repository contains the source codes used in my master's degree at PPGCCS-UFSCar.

The description of the method and its operation is available in the following presentation: https://goo.gl/kw1TrN

The full text of my MSc dissertation is available at https://repositorio.ufscar.br/handle/ufscar/9530

You may contact me by e-mail: nahimsouza@outlook.com

The code is licensed under [MIT License](LICENSE.txt).

### What? ###
* **source**: Source code - main and utils
* **resources**: Project resources - the datasets used on experiments
* **experiments**: Some benchmarks and plots from the experiments

### Requirements ###

If you want to run the experiments, follow the steps below:

- Install Python 3.X

    `sudo apt-get install python3`

- Install pip3 (i.e. pip for python3, just to install the libraries)

    `sudo apt-get install python3-pip`

- Install Scikit-Learn and auxiliar libraries

    `sudo pip3 install -U scikit-learn numpy scipy pandas matplotlib plotly imblearn`

- On *source* folder, run the experiments

    `python3 main.py`

*The commands above are considering the installation on Ubuntu-based systems, if you want to run this project on other systems, you may need to adapt them.*
